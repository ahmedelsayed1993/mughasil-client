package com.aait.mughasilclient.Network

import android.provider.ContactsContract
import com.aait.mughasilclient.Models.*
import okhttp3.MultipartBody
import retrofit2.Call
import retrofit2.http.Multipart
import retrofit2.http.POST
import retrofit2.http.Part
import retrofit2.http.Query

interface Service {
    @POST("sign-up-user")
    fun SignUp(
        @Query("phone") phone: String,
        @Query("name") name: String,
        @Query("email") email: String,
        @Query("address") address: String,
        @Query("lat") lat: String,
        @Query("lng") lng: String,
        @Query("password") password:String,
        @Query("device_id") device_id:String,
        @Query("device_type") device_type:String
    ):Call<UserResponse>

    @POST("check-code")
    fun CheckCode(@Query("user_id") user_id:Int,
                  @Query("code") code:String,
                  @Query("lang") lang:String):Call<UserResponse>

    @POST("resend-code")
    fun ReSend(@Query("user_id") user_id:Int):Call<AboutAppResponse>

    @POST("sign-in")
    fun SignIn(@Query("phone") phone:String,
               @Query("password") password:String,
               @Query("device_id") device_id: String,
               @Query("device_type") device_type: String,
               @Query("lang") lang:String):Call<UserResponse>

    @POST("about")
    fun AboutApp():Call<AboutAppResponse>


    @POST("terms")
    fun Terms():Call<AboutAppResponse>

    @POST("whatsapp")
    fun Whats():Call<AboutAppResponse>

    @POST("contact-us")
    fun Contact(@Query("name") name:String?,
                @Query("email") email:String?,
                @Query("message") message:String?):Call<ContactUsResponse>

    @POST("my-conversations")
    fun Conversations(@Query("user_id") user_id:Int,
                      @Query("lang") lang:String):Call<ChatsResponse>

    @POST("edit-profile")
    fun getProfile(@Query("user_id") user_id:Int,
                   @Query("lang") lang: String):Call<UserResponse>

    @Multipart
    @POST("edit-profile")
    fun UpdateImage(@Query("user_id") user_id:Int,
                   @Query("lang") lang: String,
                    @Part avater:MultipartBody.Part):Call<UserResponse>

    @POST("edit-profile")
    fun update(@Query("user_id") user_id:Int,
                   @Query("lang") lang: String,
               @Query("name") name:String,
               @Query("address") address:String,
               @Query("lat") lat:String,
               @Query("lng") lng: String,
               @Query("phone") phone:String):Call<UserResponse>
    @POST("edit-profile")
    fun updateLat(@Query("user_id") user_id:Int,
               @Query("lang") lang: String,
               @Query("address") address:String,
               @Query("lat") lat:String,
               @Query("lng") lng: String):Call<UserResponse>

    @POST("notifications")
    fun Notifications(@Query("user_id") user_id: Int):Call<NotificationResponse>

    @POST("reset-password")
    fun resetPassword(@Query("lang") lang:String,
                      @Query("user_id") user_id: Int,
                      @Query("current_password") current_password:String,
                      @Query("password") password:String):Call<BaseResponse>

    @POST("user-providers")
    fun Providers(@Query("user_id") user_id:Int,
                  @Query("lat") lat:String,
                  @Query("lng") lng:String,
                  @Query("text_search") text_search:String?):Call<ProvidersResponse>
    @POST("provider-details")
    fun providerDetails(@Query("provider_id") provider_id:Int):Call<ProviderDetailsResponse>


    @POST("send-order")
    fun SendOrder(@Query("user_id") user_id: Int,
                  @Query("provider_id") provider_id:Int,
                  @Query("address") address:String,
        @Query("lat") lat:String,
        @Query("lng") lng:String,
        @Query("phone") phone:String,
        @Query("time_from") time_from:String,
        @Query("time_to") time_to:String,
        @Query("date") date:String):Call<AboutAppResponse>
    @POST("edit-order")
    fun EditOrder(@Query("user_id") user_id: Int,
                  @Query("order_id") provider_id:Int,
                  @Query("address") address:String?,
                  @Query("lat") lat:String?,
                  @Query("lng") lng:String?,
                  @Query("phone") phone:String?,
                  @Query("time_from") time_from:String?,
                  @Query("time_to") time_to:String?,
                  @Query("date") date:String?):Call<EditOrderResponse>

    @POST("user-orders")
    fun Orders(@Query("user_id") user_id: Int,
               @Query("status") status:String):Call<OrdersResponse>

    @POST("conversation")
    fun Conversation(@Query("lang") lang: String,
                     @Query("conversation_id") conversation_id:Int,
                     @Query("user_id") user_id: Int,
                     @Query("page") page:Int,
                     @Query("app_type") app_type:String):Call<ChatResponse>

    @POST("conversation-id")
    fun ConversationID(@Query("user_id") user_id:Int,
                       @Query("provider_id") provider_id:Int):Call<ConversationIdResponse>

    @POST("send-message")
    fun Send(@Query("lang") lang: String,
             @Query("user_id") user_id: Int,
             @Query("receiver_id") receiver_id:Int,
             @Query("conversation_id") conversation_id:Int,
             @Query("message") message:String):Call<SendChatResponse>

    @POST("log-out")
    fun logOut(@Query("user_id") user_id:Int,
               @Query("device_id") device_id:String,
               @Query("device_type") device_type:String,
               @Query("lang") lang:String):Call<AboutAppResponse>
    @POST("send-order-providers")
    fun SendProvider(@Query("user_id") user_id: Int,
                     @Query("lat") lat: String,
                     @Query("lng") lng: String,
                     @Query("address") address: String):Call<AboutAppResponse>

    @POST("forget-password")
    fun ForGot(@Query("phone") phone:String,
               @Query("lang") lang:String):Call<UserResponse>

    @POST("update-password")
    fun NewPass(@Query("user_id") user_id: Int,
                @Query("password") password:String,
                @Query("code") code:String,
                @Query("lang") lang: String):Call<UserResponse>

    @POST("show-phone")
    fun switchPhone(@Query("user_id") user_id:Int,
               @Query("switch") switch:Int?):Call<SwitchNotification>
    @POST("switch-notification")
    fun switch(@Query("user_id") user_id:Int,
               @Query("switch") switch:Int?):Call<SwitchNotification>
    @POST("delete-notification")
    fun delete(
        @Query("user_id") user_id:Int,
        @Query("notification_id") notification_id:Int):Call<AboutAppResponse>
    @POST("details-order")
    fun OrderDetails(@Query("provider_id") provider_id:Int,
                     @Query("order_id") order_id:Int,
                     @Query("user_type") user_type:String,
                     @Query("action") action:String?):Call<OrderDetailsResponse>

    @POST("trsck-order-user")
    fun track(@Query("user_id") user_id: Int,
              @Query("order_id") order_id: Int):Call<AboutAppResponse>

    @POST("user-delete-order")
    fun deleteOrder(@Query("user_id") user_id:Int,
                    @Query("order_id") order_id:Int
                   ):Call<AboutAppResponse>
}