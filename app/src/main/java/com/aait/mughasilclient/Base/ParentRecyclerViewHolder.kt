package com.aait.mughasilclient.Base


import android.view.View

import androidx.recyclerview.widget.RecyclerView






/**
 * is a base class to extend from it the viewholder
 */
open class ParentRecyclerViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    private var clickableRootView: View? =
        null // this is used to change the default onItemClickListener

    init {


    }

//    fun setOnItemClickListener(itemClickListener: OnItemClickListener?) {
//        if (clickableRootView != null) {
//            clickableRootView!!.setOnClickListener { v ->
//                if (itemClickListener != null) {
//                    itemClickListener!!.onItemClick(v, adapterPosition)
//                }
//            }
//        } else {
//            itemView.setOnClickListener { v ->
//                if (itemClickListener != null) {
//                    itemClickListener!!.onItemClick(v, adapterPosition)
//                }
//            }
//        }
//    }

    fun setClickableRootView(clickableRootView: View) {
        this.clickableRootView = clickableRootView
    }

    fun findViewById(viewId: Int): View? {
        return itemView?.findViewById(viewId)
    }
}
