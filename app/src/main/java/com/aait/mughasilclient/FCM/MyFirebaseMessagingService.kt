package com.aait.mughasilclient.FCM

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.media.RingtoneManager
import android.os.Build
import android.util.Log
import androidx.core.app.NotificationCompat
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.aait.mughasilclient.Perefereces.LanguagePrefManager
import com.aait.mughasilclient.Perefereces.SharedPrefManager
import com.aait.mughasilclient.R
import com.aait.mughasilclient.UI.Activities.ChatsActivity
import com.aait.mughasilclient.UI.Activities.NotificationActivity
import com.aait.mughasilclient.UI.Activities.SplashActivity
import com.aait.mughasilclient.Uitls.CommonUtil

import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.google.gson.Gson

/**
 * Created by aait on 11/4/2019.
 */
class MyFirebaseMessagingService : FirebaseMessagingService() {
    internal lateinit var sharedPreferences: SharedPreferences
    private val TAG = "FCM Messaging"

    internal var notificationTitle: String? = null

    internal var notificationMessage: String? = null

    internal var notificationData: String? = null

    internal lateinit var mSharedPrefManager: SharedPrefManager

    internal var notificationType: String? = null

    // OrderModel mOrderModel;

    internal lateinit var mLanguagePrefManager: LanguagePrefManager

    override fun onNewToken(s: String?) {
        super.onNewToken(s)
        Log.e("NEW_TOKEN", s!!)
    }

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        sharedPreferences = getSharedPreferences("home", Context.MODE_PRIVATE)
//        remoteMessage.notification?.let { CommonUtil.onPrintLog(it) }
//        CommonUtil.onPrintLog(Gson().toJson(remoteMessage.notification))
        CommonUtil.onPrintLog(remoteMessage.data)
        CommonUtil.onPrintLog(remoteMessage.originalPriority)
        CommonUtil.onPrintLog(remoteMessage.priority)
        CommonUtil.onPrintLog(remoteMessage.sentTime)
        CommonUtil.onPrintLog(remoteMessage.ttl)
        mSharedPrefManager = SharedPrefManager(applicationContext)
        mLanguagePrefManager = LanguagePrefManager(applicationContext)

        //        if (!mSharedPrefManager.getUserData().getNotification_status().equals("")) {
        //            return;
        //        }


        //        notificationType = remoteMessage.getData().get("type");
        notificationTitle = remoteMessage.data["title"]
        notificationMessage = remoteMessage.data["body"]
        notificationType = remoteMessage.data["type"]
        Log.e("type",notificationType)
        // if the notification contains data payload
        if (remoteMessage == null) {
            return
        }

        // if the user not logged in never do any thing
        if (!mSharedPrefManager.loginStatus!!) {
            return
        } else {
            if (remoteMessage.data["type"] == "notification") {
                val intent = Intent(this, NotificationActivity::class.java)
                showNotification(remoteMessage, intent)
            }else if (remoteMessage.data["type"] == "block") {
                mSharedPrefManager.loginStatus=false
                mSharedPrefManager.Logout()
                startActivity(Intent(this, SplashActivity::class.java))

            }else if (remoteMessage.data["type"] == "user_deleted") {
                mSharedPrefManager.loginStatus=false
                mSharedPrefManager.Logout()
                startActivity(Intent(this, SplashActivity::class.java))

            }
            else if (remoteMessage.data["type"] == "chat") {
                if (sharedPreferences.getString("sender_id", "0") == remoteMessage.data["conversation_id"]) {
                    val intent = Intent("new_message")
                    intent.putExtra("msg", remoteMessage.data["message"])
                    Log.e("msg", remoteMessage.data["message"]!!)

                    intent.putExtra("receiver_id", remoteMessage.data["receiver_id"])

                    intent.putExtra("avatar", remoteMessage.data["avatar"])

                    intent.putExtra("conversation_id", remoteMessage.data["conversation_id"])

                    intent.putExtra("user_id", remoteMessage.data["user_id"])
                    intent.putExtra("id", remoteMessage.data["id"])

                    intent.putExtra("created", remoteMessage.data["created"])


                    LocalBroadcastManager.getInstance(applicationContext).sendBroadcast(intent)
                }else{
                    val intent = Intent(this, ChatsActivity::class.java)
                    showNotification(remoteMessage, intent)
                }
            }else{
                val intent = Intent(this, NotificationActivity::class.java)
                showNotification(remoteMessage, intent)
            }
        }
    }

    private fun showNotification(message: RemoteMessage, intent: Intent) {

        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK


        val pendingIntent = PendingIntent.getActivity(
            this,
            0,
            intent,
            PendingIntent.FLAG_ONE_SHOT
        )

        val myNotification = NotificationCompat.Builder(this)
            .setContentTitle(message.data["title"])
            .setContentText(message.data["body"])
            .setTicker("Notification!")
            .setWhen(System.currentTimeMillis())
            .setContentIntent(pendingIntent)
            .setDefaults(Notification.DEFAULT_VIBRATE)
            .setAutoCancel(true)
            .setPriority(Notification.PRIORITY_MAX)
            .setSmallIcon(R.mipmap.logo)



        val notificationManager =
            this.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val importance = NotificationManager.IMPORTANCE_HIGH
            val notificationChannel =
                NotificationChannel("10001", "NOTIFICATION_CHANNEL_NAME", importance)
            notificationChannel.enableLights(true)
            notificationChannel.lightColor = Color.DKGRAY
            notificationChannel.setShowBadge(true)
            notificationChannel.enableVibration(true)
            notificationChannel.vibrationPattern =
                longArrayOf(100, 200, 300, 400, 500, 400, 300, 200, 400)
            myNotification.setChannelId("10001")
            notificationManager.createNotificationChannel(notificationChannel)
        }
        notificationManager.notify(0 /* Request Code */, myNotification.build())
    }

}