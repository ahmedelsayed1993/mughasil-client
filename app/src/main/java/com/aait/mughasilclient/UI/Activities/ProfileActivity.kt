package com.aait.mughasilclient.UI.Activities

import android.app.Dialog
import android.content.Intent
import android.os.Build
import android.text.InputType
import android.util.Log
import android.view.ViewGroup
import android.view.Window
import android.widget.*
import com.aait.mughasilclient.Base.Parent_Activity
import com.aait.mughasilclient.Client
import com.aait.mughasilclient.Models.BaseResponse
import com.aait.mughasilclient.Models.UserModel
import com.aait.mughasilclient.Models.UserResponse
import com.aait.mughasilclient.Network.Service
import com.aait.mughasilclient.R
import com.aait.mughasilclient.Uitls.CommonUtil
import com.aait.mughasilclient.Uitls.PermissionUtils
import com.bumptech.glide.Glide
import com.fxn.pix.Options
import com.fxn.pix.Pix
import com.fxn.utility.ImageQuality
import com.google.gson.Gson
import de.hdodenhof.circleimageview.CircleImageView
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File

class ProfileActivity:Parent_Activity() {
    override val layoutResource: Int
        get() = R.layout.activity_profile
    lateinit var menu:ImageView
    lateinit var back:ImageView
    lateinit var title:TextView
    lateinit var image:ImageView

    lateinit var name:EditText

    lateinit var address:TextView

    lateinit var phone:EditText

    lateinit var change_password:LinearLayout
    lateinit var password:TextView
    lateinit var confirm:Button
    var lat = ""
    var lng = ""
    var location = ""
    internal var returnValue: java.util.ArrayList<String>? = java.util.ArrayList()
    internal var options = Options.init()
        .setRequestCode(100)                                                 //Request code for activity results
        .setCount(1)                                                         //Number of images to restict selection count
        .setFrontfacing(false)                                                //Front Facing camera on start
        .setImageQuality(ImageQuality.HIGH)                                  //Image Quality
        .setPreSelectedUrls(returnValue)                                     //Pre selected Image Urls
        .setScreenOrientation(Options.SCREEN_ORIENTATION_PORTRAIT)           //Orientaion
        .setPath("/pix/images")
    private var ImageBasePath: String? = null

    override fun initializeComponents() {
        menu = findViewById(R.id.menu)
        back = findViewById(R.id.back)
        title = findViewById(R.id.title)
        image = findViewById(R.id.image)

        name = findViewById(R.id.name)

        address = findViewById(R.id.address)

        phone = findViewById(R.id.phone)

        change_password = findViewById(R.id.change_password)
        password = findViewById(R.id.password)
        confirm = findViewById(R.id.confirm)
        title.text = getString(R.string.profile)
        menu.setOnClickListener { startActivity(Intent(this,MainActivity::class.java))
            finish()}
        back.setOnClickListener { startActivity(Intent(this,NotificationActivity::class.java))
            finish()}
        Profile()

        image.setOnClickListener {
            if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
                if (!(PermissionUtils.hasPermissions(mContext,
                        android.Manifest.permission.CAMERA)&& PermissionUtils.hasPermissions(mContext,
                        android.Manifest.permission.READ_EXTERNAL_STORAGE)&& PermissionUtils.hasPermissions(mContext,
                        android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                            )) {
                    CommonUtil.PrintLogE("Permission not granted")
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        requestPermissions(
                            PermissionUtils.IMAGE_PERMISSIONS,
                            400
                        )
                    }
                } else {
                    Pix.start(this, options)
                    CommonUtil.PrintLogE("Permission is granted before")
                }
            } else {
                CommonUtil.PrintLogE("SDK minimum than 23")
                Pix.start(this, options)
            }
        }
        address.setOnClickListener { startActivityForResult(Intent(this@ProfileActivity,LocationActivity::class.java),1) }

        confirm.setOnClickListener {
            if ( CommonUtil.checkEditError(name,getString(R.string.name))||
                CommonUtil.checkTextError(address,getString(R.string.location))||
                CommonUtil.checkEditError(phone,getString(R.string.phone_number))||
                CommonUtil.checkLength1(phone,getString(R.string.phone_length),10)){
                return@setOnClickListener
            }else{
                update()
            }
        }

        change_password.setOnClickListener {
            val dialog = Dialog(mContext)
            dialog?.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog?.window!!.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
            dialog ?.setCancelable(true)
            dialog ?.setContentView(R.layout.dialog_change_password)
            val old_pass = dialog?.findViewById<EditText>(R.id.old_pass)
            val new_pass = dialog?.findViewById<EditText>(R.id.new_pass)
            val confirm_pass = dialog?.findViewById<EditText>(R.id.confirm_pass)
            val view = dialog?.findViewById<ImageView>(R.id.view)
            val view_pass = dialog?.findViewById<ImageView>(R.id.view1)
            val view_confirm = dialog?.findViewById<ImageView>(R.id.view2)
            val save = dialog?.findViewById<Button>(R.id.confirm)
            view?.setOnClickListener { if (old_pass?.inputType== InputType.TYPE_TEXT_VARIATION_NORMAL or InputType.TYPE_CLASS_TEXT){
                old_pass?.inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD

            }else{
                old_pass?.inputType = InputType.TYPE_TEXT_VARIATION_NORMAL or InputType.TYPE_CLASS_TEXT
            }
            }
            view_pass?.setOnClickListener { if (new_pass?.inputType== InputType.TYPE_TEXT_VARIATION_NORMAL or InputType.TYPE_CLASS_TEXT){
                new_pass?.inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD

            }else{
                new_pass?.inputType = InputType.TYPE_TEXT_VARIATION_NORMAL or InputType.TYPE_CLASS_TEXT
            }
            }
            view_confirm?.setOnClickListener { if (confirm_pass?.inputType== InputType.TYPE_TEXT_VARIATION_NORMAL or InputType.TYPE_CLASS_TEXT){
                confirm_pass?.inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD

            }else{
                confirm_pass?.inputType = InputType.TYPE_TEXT_VARIATION_NORMAL or InputType.TYPE_CLASS_TEXT
            }
            }

            save?.setOnClickListener {
                if (CommonUtil.checkEditError(old_pass,getString(R.string.old_password))||
                    CommonUtil.checkEditError(new_pass,getString(R.string.new_password))||
                    CommonUtil.checkLength(new_pass,getString(R.string.password_length),6)||
                    CommonUtil.checkEditError(confirm_pass,getString(R.string.confirm_password))){
                    return@setOnClickListener
                }else{
                    if (!new_pass.text.toString().equals(confirm_pass.text.toString())){
                        confirm_pass.error = getString(R.string.password_not_match)
                    }else{
                        showProgressDialog(getString(R.string.please_wait))
                        Client.getClient()?.create(Service::class.java)?.resetPassword(mLanguagePrefManager.appLanguage,mSharedPrefManager.userData.id!!,old_pass.text.toString(),new_pass.text.toString())?.enqueue(
                            object :Callback<BaseResponse>{
                                override fun onFailure(call: Call<BaseResponse>, t: Throwable) {
                                    CommonUtil.handleException(mContext,t)
                                    t.printStackTrace()
                                    hideProgressDialog()
                                }

                                override fun onResponse(
                                    call: Call<BaseResponse>,
                                    response: Response<BaseResponse>
                                ) {
                                    hideProgressDialog()
                                    if (response.isSuccessful){
                                        if (response.body()?.value.equals("1")){
                                            CommonUtil.makeToast(mContext,response.body()?.msg!!)
                                            dialog?.dismiss()
                                        }else{
                                            CommonUtil.makeToast(mContext,response.body()?.msg!!)

                                        }
                                    }
                                }
                            }
                        )
                    }
                }

            }
            dialog?.show()
        }

    }
    fun setData(userModel: UserModel) {
        mSharedPrefManager.userData = userModel
        Glide.with(mContext).asBitmap().load(userModel.avatar).into(image)
        name.setText(userModel.name!!)
        address.text = userModel.address
        location = userModel.address!!
        lat = userModel.lat!!
        lng = userModel.lng!!
        phone.setText(userModel.phone!!)

    }

    fun  Profile(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.getProfile(mSharedPrefManager.userData.id!!,mLanguagePrefManager.appLanguage)?.enqueue(object :
            Callback<UserResponse>{
            override fun onFailure(call: Call<UserResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(call: Call<UserResponse>, response: Response<UserResponse>) {
                hideProgressDialog()
                if (response.body()?.value.equals("1")){
                    setData(response.body()?.data!!)
                    mSharedPrefManager.userData = response.body()?.data!!
                }else{
                    CommonUtil.makeToast(mContext,response.body()?.msg!!)
                }
            }

        })
    }
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 100) {
            if (resultCode==0){


            }else {
                returnValue = data!!.getStringArrayListExtra(Pix.IMAGE_RESULTS)
                 Log.e("data", Gson().toJson(returnValue))
                ImageBasePath = returnValue!![0]


                Glide.with(mContext).load(ImageBasePath).into(image)

                if (ImageBasePath != null) {
                    upLoad(ImageBasePath!!)
                    //ID.text = getString(R.string.Image_attached)
                }
            }
        }else{
            if (data?.getStringExtra("result")!=null) {
                location = data?.getStringExtra("result").toString()
                lat = data?.getStringExtra("lat").toString()
                lng = data?.getStringExtra("lng").toString()
                address.text = location
            }else{
                location = ""
                lat = ""
                lng = ""
                address.text = location
            }
        }
    }

    fun upLoad(path:String){
        showProgressDialog(getString(R.string.please_wait))
        var filePart: MultipartBody.Part? = null
        val ImageFile = File(path)
        val fileBody = RequestBody.create(MediaType.parse("*/*"), ImageFile)
        filePart = MultipartBody.Part.createFormData("avatar", ImageFile.name, fileBody)
        Client.getClient()?.create(Service::class.java)?.UpdateImage(mSharedPrefManager.userData.id!!,mLanguagePrefManager.appLanguage,filePart)?.enqueue(object :Callback<UserResponse>{
            override fun onFailure(call: Call<UserResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(call: Call<UserResponse>, response: Response<UserResponse>) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        setData(response.body()?.data!!)
                        mSharedPrefManager.userData = response.body()?.data!!
                        startActivity(Intent(this@ProfileActivity,MainActivity::class.java))
                        this@ProfileActivity.finish()
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }
    fun update(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.update(mSharedPrefManager.userData.id!!,mLanguagePrefManager.appLanguage,name.text.toString()
        ,address.text.toString(),lat,lng,phone.text.toString())?.enqueue(object :Callback<UserResponse>{
            override fun onFailure(call: Call<UserResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(call: Call<UserResponse>, response: Response<UserResponse>) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        setData(response.body()?.data!!)
                        mSharedPrefManager.userData = response.body()?.data!!
                        startActivity(Intent(this@ProfileActivity,MainActivity::class.java))
                        this@ProfileActivity.finish()
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }
}