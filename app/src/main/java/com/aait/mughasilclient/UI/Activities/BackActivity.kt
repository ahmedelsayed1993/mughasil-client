package com.aait.mughasilclient.UI.Activities

import android.content.Intent
import android.widget.Button
import com.aait.mughasilclient.Base.Parent_Activity
import com.aait.mughasilclient.R

class BackActivity :Parent_Activity(){
    override val layoutResource: Int
        get() = R.layout.activity_back
    lateinit var back: Button
    override fun initializeComponents() {
        back = findViewById(R.id.back)
        back.setOnClickListener { startActivity(Intent(this,MainActivity::class.java))
            finish()}
    }
    override fun onBackPressed() {
        super.onBackPressed()
        startActivity(Intent(this,MainActivity::class.java))
        finish()
    }
}