package com.aait.mughasilclient.UI.Activities

import android.app.Dialog
import android.content.Intent
import android.view.ViewGroup
import android.view.Window
import android.widget.TextView
import com.aait.mughasilclient.Base.Parent_Activity
import com.aait.mughasilclient.Client
import com.aait.mughasilclient.Models.AboutAppResponse
import com.aait.mughasilclient.Network.Service
import com.aait.mughasilclient.R
import com.aait.mughasilclient.Uitls.CommonUtil
import org.w3c.dom.Text
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class LoadActivity:Parent_Activity() {
    override val layoutResource: Int
        get() = R.layout.activity_loding

    var lat= ""
    var lng = ""
    var address = ""
    override fun initializeComponents() {
        lat = intent.getStringExtra("lat")
        lng = intent.getStringExtra("lng")
        address = intent.getStringExtra("address")

        val dialog = Dialog(mContext)
        dialog?.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog?.window!!.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
        dialog ?.setCancelable(false)
        dialog ?.setContentView(R.layout.dialog_order)
        val confirm = dialog?.findViewById<TextView>(R.id.confirm)
        val back = dialog?.findViewById<TextView>(R.id.back)

        confirm.setOnClickListener {
            showProgressDialog(getString(R.string.please_wait))
            Client.getClient()?.create(Service::class.java)
                ?.SendProvider(mSharedPrefManager.userData.id!!, lat, lng, address)
                ?.enqueue(object :
                    Callback<AboutAppResponse> {
                    override fun onFailure(call: Call<AboutAppResponse>, t: Throwable) {
                        CommonUtil.handleException(mContext, t)
                        t.printStackTrace()
                        hideProgressDialog()

                    }

                    override fun onResponse(
                        call: Call<AboutAppResponse>,
                        response: Response<AboutAppResponse>
                    ) {
                        hideProgressDialog()
                        if (response.isSuccessful) {
                            if (response.body()?.value.equals("1")) {
                                CommonUtil.makeToast(mContext,response.body()?.data!!)
                                startActivity(Intent(this@LoadActivity, BackActivity::class.java))
                                finish()
                            } else {
                                CommonUtil.makeToast(mContext,response.body()?.msg!!)
                                startActivity(Intent(this@LoadActivity, MainActivity::class.java))
                                finish()
                            }
                        }
                    }

                })
        }
        back.setOnClickListener {
            dialog?.dismiss()
            startActivity(Intent(this@LoadActivity,MainActivity::class.java))
            finish()
        }
        dialog?.show()


    }
}