package com.aait.mughasilclient.UI.Adapters

import android.content.Context

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter


import com.aait.mughasilclient.UI.Fragments.CurrentOrdersFragments
import com.aait.mughasilclient.UI.Fragments.FinishedOrdersFragment
import com.aait.mughasilclient.UI.Fragments.OrdersFragment


class SubscribeTapAdapter(
    private val context: Context,
    fm: FragmentManager
) : FragmentPagerAdapter(fm) {

    override fun getItem(position: Int): Fragment {
        return if (position == 0) {
            OrdersFragment()
        }else if (position == 1){
            CurrentOrdersFragments()
        } else {
            FinishedOrdersFragment()
        }
    }

    override fun getCount(): Int {
        return 3
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return if (position == 0) {
            "قيد الاعتماد"
        }else if(position ==1){
            "الطلبات الحالية"
        } else {
            "الطلبات السابقة"
        }
    }
}
