package com.aait.mughasilclient.UI.Activities

import android.content.Intent
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import android.widget.ToggleButton
import com.aait.mughasilclient.Base.Parent_Activity
import com.aait.mughasilclient.Client
import com.aait.mughasilclient.Models.SwitchNotification
import com.aait.mughasilclient.Network.Service
import com.aait.mughasilclient.R
import com.aait.mughasilclient.Uitls.CommonUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class SettingsActivity :Parent_Activity(){
    override val layoutResource: Int
        get() = R.layout.activity_settings
    lateinit var back:ImageView
    lateinit var title:TextView
    lateinit var menu:ImageView
    lateinit var notification:ToggleButton
    lateinit var phone:ToggleButton

    override fun initializeComponents() {
        back = findViewById(R.id.back)
        title = findViewById(R.id.title)
        menu = findViewById(R.id.menu)
        notification = findViewById(R.id.notification)
        phone = findViewById(R.id.phone)
        back.visibility = View.GONE
        menu.setOnClickListener { startActivity(Intent(this,MainActivity::class.java))
            finish()}
        back.setOnClickListener { startActivity(Intent(this,NotificationActivity::class.java))
            finish()}
        title.text = getString(R.string.settings)
        switch()
        switchPhone()
        notification.setOnClickListener {
            if (notification.isChecked){
                switch(1)
            }else{
                switch(0)
            }
        }
        phone.setOnClickListener {
            if (phone.isChecked){
                switchPhone(1)
            }else{
                switchPhone(0)
            }
        }

    }

    fun switch(state:Int){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.switch(mSharedPrefManager.userData.id!!,state)?.enqueue(object :
            Callback<SwitchNotification> {
            override fun onFailure(call: Call<SwitchNotification>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(
                call: Call<SwitchNotification>,
                response: Response<SwitchNotification>
            ) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        startActivity(Intent(this@SettingsActivity,MainActivity::class.java))
                        finish()
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }
    fun switchPhone(state:Int){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.switchPhone(mSharedPrefManager.userData.id!!,state)?.enqueue(object :
            Callback<SwitchNotification> {
            override fun onFailure(call: Call<SwitchNotification>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(
                call: Call<SwitchNotification>,
                response: Response<SwitchNotification>
            ) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        startActivity(Intent(this@SettingsActivity,MainActivity::class.java))
                        finish()
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }
    fun switch(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.switch(mSharedPrefManager.userData.id!!,null)?.enqueue(object :
            Callback<SwitchNotification> {
            override fun onFailure(call: Call<SwitchNotification>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(
                call: Call<SwitchNotification>,
                response: Response<SwitchNotification>
            ) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        if (response.body()?.data==1){
                            notification.isChecked = true
                        }else{
                            notification.isChecked = false
                        }

                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }
    fun switchPhone(){

        Client.getClient()?.create(Service::class.java)?.switchPhone(mSharedPrefManager.userData.id!!,null)?.enqueue(object :
            Callback<SwitchNotification> {
            override fun onFailure(call: Call<SwitchNotification>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(
                call: Call<SwitchNotification>,
                response: Response<SwitchNotification>
            ) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        if (response.body()?.data==1){
                            phone.isChecked = true
                        }else{
                            phone.isChecked = false
                        }

                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }
}