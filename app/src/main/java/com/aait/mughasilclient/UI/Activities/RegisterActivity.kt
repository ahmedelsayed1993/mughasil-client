package com.aait.mughasilclient.UI.Activities

import android.content.Intent
import android.widget.Button
import android.widget.CheckBox
import android.widget.EditText
import android.widget.TextView
import com.aait.mughasilclient.Base.Parent_Activity
import com.aait.mughasilclient.Client
import com.aait.mughasilclient.Models.UserResponse
import com.aait.mughasilclient.Network.Service
import com.aait.mughasilclient.R
import com.aait.mughasilclient.Uitls.CommonUtil
import com.google.firebase.iid.FirebaseInstanceId
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class RegisterActivity:Parent_Activity() {
    override val layoutResource: Int
        get() = R.layout.activity_register
    lateinit var register:Button
    lateinit var phone:EditText
    lateinit var name:EditText
    lateinit var mail:EditText
    lateinit var location:TextView
    lateinit var password:EditText
    lateinit var confirm:EditText
    var lat = ""
    var lng = ""
    var addresses = ""
    var deviceID = ""
    var email = ""
    var userName = ""
    lateinit var terms: CheckBox

    override fun initializeComponents() {
        deviceID = FirebaseInstanceId.getInstance().token.toString()
        email = intent.getStringExtra("mail")
        userName = intent.getStringExtra("name")
        register = findViewById(R.id.register)
        phone = findViewById(R.id.phone)
        name = findViewById(R.id.name)
        mail = findViewById(R.id.mail)
        location = findViewById(R.id.location)
        password = findViewById(R.id.password)
        confirm = findViewById(R.id.confirm)
        terms = findViewById(R.id.terms)
        mail.setText(email)
        name.setText(userName)
        location.setOnClickListener { startActivityForResult(Intent(this@RegisterActivity,LocationActivity::class.java),1) }
        register.setOnClickListener {
            if (CommonUtil.checkEditError(phone,getString(R.string.phone_number))||
                    CommonUtil.checkLength1(phone,getString(R.string.phone_length),10)||
                    CommonUtil.checkEditError(name,getString(R.string.name))||
                    CommonUtil.checkEditError(mail,getString(R.string.email))||
                   ! CommonUtil.isEmailValid(mail,getString(R.string.correct_email))||
                    CommonUtil.checkTextError(location,getString(R.string.location))||
                    CommonUtil.checkEditError(password,getString(R.string.password))||
                    CommonUtil.checkLength(password,getString(R.string.password_length),6)||
                    CommonUtil.checkEditError(confirm,getString(R.string.confirm_password))){
                return@setOnClickListener
            }else{
                if (!password.text.toString().equals(confirm.text.toString())){
                    confirm.error = getString(R.string.password_not_match)
                }else{
                    if (terms.isChecked) {
                        signUp()
                    }else{
                        CommonUtil.makeToast(mContext,"الموافقة على الشروط وسياسة التطبيق")
                    }
                }
            }
        }

        terms.setOnClickListener {
            val intent = Intent(this,TermsActivity::class.java)
            intent.putExtra("type","one")
            startActivity(intent)

        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (data?.getStringExtra("result")!=null) {
            addresses = data?.getStringExtra("result").toString()
            lat = data?.getStringExtra("lat").toString()
            lng = data?.getStringExtra("lng").toString()
            location.text = addresses
        }else{
            addresses = ""
            lat = ""
            lng = ""
            location.text = addresses
        }
    }

    fun signUp(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.SignUp(phone.text.toString(),name.text.toString(),mail.text.toString(),location.text.toString(),lat,lng,password.text.toString(),deviceID,"android")?.enqueue(object :
            Callback<UserResponse>{
            override fun onFailure(call: Call<UserResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(call: Call<UserResponse>, response: Response<UserResponse>) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        val intent  = Intent(this@RegisterActivity,ActiviationActivity::class.java)
                        intent.putExtra("data",response.body()?.data)
                        startActivity(intent)
                        finish()
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }
        })
    }
}