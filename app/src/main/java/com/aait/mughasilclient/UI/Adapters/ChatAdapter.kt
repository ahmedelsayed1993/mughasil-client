package com.aait.mughasilclient.UI.Adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView


import com.aait.mughasilclient.Models.ChatModel
import com.aait.mughasilclient.R
import com.bumptech.glide.Glide

import de.hdodenhof.circleimageview.CircleImageView

class ChatAdapter(
    internal var context: Context,
    internal var messageModels: List<ChatModel>,
    internal var id: Int
) : RecyclerView.Adapter<ChatAdapter.ViewHolder>() {
    internal var paramsMsg: LinearLayout.LayoutParams? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v: View
        return if (viewType == 0) {
            ViewHolder(
                LayoutInflater.from(parent.context).inflate(
                    R.layout.card_chat_send,
                    parent,
                    false
                )
            )
        } else
            ViewHolder(
                LayoutInflater.from(parent.context).inflate(
                    R.layout.card_chat_recieve,
                    parent,
                    false
                )
            )
    }

    override fun getItemViewType(position: Int): Int {

        return if (messageModels[position].user_id == id) {

            1
        } else {

            0
        }

    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder.msg.text = messageModels[position].message
        holder.date.text = messageModels[position].created
        Glide.with(context).load(messageModels[position].avatar).into(holder.image)


    }

    override fun getItemCount(): Int {
        return messageModels.size
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        internal var msg: TextView
        internal var date: TextView
        internal var image: CircleImageView


        init {
            msg = itemView.findViewById(R.id.tv_name)
            date = itemView.findViewById(R.id.tv_date)
            image = itemView.findViewById(R.id.sender)


        }
    }
}
