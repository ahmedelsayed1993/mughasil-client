package com.aait.mughasilclient.UI.Activities

import android.content.Intent
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.widget.Button
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.TextView
import com.aait.mughasilclient.Base.Parent_Activity
import com.aait.mughasilclient.Client
import com.aait.mughasilclient.Models.AboutAppResponse
import com.aait.mughasilclient.Models.UserModel
import com.aait.mughasilclient.Models.UserResponse
import com.aait.mughasilclient.Network.Service
import com.aait.mughasilclient.R
import com.aait.mughasilclient.Uitls.CommonUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ActiviationActivity :Parent_Activity(){
    override val layoutResource: Int
        get() = R.layout.activity_activiation_code

    lateinit var one:EditText
    lateinit var two:EditText
    lateinit var three:EditText
    lateinit var four:EditText
    lateinit var confirm:Button
    lateinit var send:TextView
    lateinit var lay:LinearLayout
    var code = ""
    lateinit var userModel: UserModel
    override fun initializeComponents() {
        userModel = intent.getSerializableExtra("data") as UserModel
        one = findViewById(R.id.one)
        two = findViewById(R.id.two)
        three = findViewById(R.id.three)
        four = findViewById(R.id.four)
        confirm = findViewById(R.id.confirm)
        send = findViewById(R.id.send)
        lay = findViewById(R.id.lay)
        code = one.text.toString()+two.text.toString()+three.text.toString()+four.text.toString()
        Log.e("code",code)
        one.setSelection(0)
        two.setSelection(0)
        three.setSelection(0)
        four.setSelection(0)
        one.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
                two.requestFocus()
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

        })
        two.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
                three.requestFocus()
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

        })
        three.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
                four.requestFocus()
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

        })
        confirm.setOnClickListener {
            if (CommonUtil.checkEditError(one,getString(R.string.activation_code))||
                CommonUtil.checkEditError(two,getString(R.string.activation_code))||
                CommonUtil.checkEditError(three,getString(R.string.activation_code))||
                CommonUtil.checkEditError(four,getString(R.string.activation_code))){
                return@setOnClickListener
            }else{
                var code = one.text.toString()+two.text.toString()+three.text.toString()+four.text.toString()
                check(code)

            }
        }
        send.setOnClickListener { resend() }


    }
    fun check(code:String){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.CheckCode(userModel.id!!,code,"ar")?.enqueue(object :
            Callback<UserResponse> {
            override fun onFailure(call: Call<UserResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(call: Call<UserResponse>, response: Response<UserResponse>) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        mSharedPrefManager.userData = response.body()?.data!!
                        mSharedPrefManager.loginStatus = true
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                        val intent = Intent(this@ActiviationActivity,MainActivity::class.java)
                        startActivity(intent)
                        finish()
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }

    fun resend(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.ReSend(userModel.id!!)?.enqueue(object :Callback<AboutAppResponse>{
            override fun onFailure(call: Call<AboutAppResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(
                call: Call<AboutAppResponse>,
                response: Response<AboutAppResponse>
            ) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        CommonUtil.makeToast(mContext,response.body()?.data!!)
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }

}