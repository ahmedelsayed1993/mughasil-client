package com.aait.mughasilclient.UI.Activities

import android.content.Intent
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import com.aait.mughasilclient.Base.Parent_Activity
import com.aait.mughasilclient.R

class OneActivity:Parent_Activity() {
    override val layoutResource: Int
        get() =  R.layout.activity_one
    lateinit var skip: Button

    override fun initializeComponents() {
        skip = findViewById(R.id.skip)


        skip.setOnClickListener {
            startActivity(Intent(this@OneActivity,GoogleActivity::class.java))
            finish()
        }

    }
}