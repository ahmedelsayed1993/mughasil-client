package com.aait.mughasilclient.UI.Activities

import android.os.Build
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.RequiresApi
import com.aait.mughasilclient.Base.Parent_Activity
import com.aait.mughasilclient.Client
import com.aait.mughasilclient.Models.AboutAppResponse
import com.aait.mughasilclient.Network.Service
import com.aait.mughasilclient.R
import com.aait.mughasilclient.Uitls.CommonUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class TrackOrderActivity:Parent_Activity() {
    override val layoutResource: Int
        get() = R.layout.activity_track_order

    lateinit var back:ImageView
    lateinit var title:TextView
    lateinit var one:ImageView
    lateinit var two:ImageView
    lateinit var three:ImageView
    var id = 0

    override fun initializeComponents() {
        id = intent.getIntExtra("id",0)

        back = findViewById(R.id.back)
        title = findViewById(R.id.title)
        one = findViewById(R.id.one)
        two = findViewById(R.id.two)
        three = findViewById(R.id.three)

        back.setOnClickListener { onBackPressed()
        finish()}
        title.text = "متابعة الطلب"
        Track()

    }

    fun Track(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.track(mSharedPrefManager.userData.id!!,id)?.enqueue(object :
            Callback<AboutAppResponse> {
            override fun onFailure(call: Call<AboutAppResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
            override fun onResponse(
                call: Call<AboutAppResponse>,
                response: Response<AboutAppResponse>
            ) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        if (response.body()?.data.equals("accepted")||response.body()?.data.equals("pending")){
                            one.background =mContext.getDrawable(R.drawable.white_perple_circle)
                            two.background =mContext.getDrawable(R.drawable.white_perple_circle)
                            three.background =mContext.getDrawable(R.drawable.white_perple_circle)
                            one.setImageResource(R.mipmap.path222)
                        }else if (response.body()?.data.equals("work_underway")){
                            one.background =mContext.getDrawable(R.drawable.white_perple_circle)
                            two.background =mContext.getDrawable(R.drawable.white_perple_circle)
                            three.background =mContext.getDrawable(R.drawable.white_perple_circle)
                            one.setImageResource(R.mipmap.path222)
                            two.setImageResource(R.mipmap.path222)
                        }else if (response.body()?.data.equals("completed")){
                            one.background =mContext.getDrawable(R.drawable.white_perple_circle)
                            two.background =mContext.getDrawable(R.drawable.white_perple_circle)
                            three.background =mContext.getDrawable(R.drawable.white_perple_circle)
                            one.setImageResource(R.mipmap.path222)
                            two.setImageResource(R.mipmap.path222)
                            three.setImageResource(R.mipmap.path222)
                        }else{
                            one.background =mContext.getDrawable(R.drawable.white_perple_circle)
                            two.background =mContext.getDrawable(R.drawable.white_perple_circle)
                            three.background =mContext.getDrawable(R.drawable.white_perple_circle)
                        }
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }
}