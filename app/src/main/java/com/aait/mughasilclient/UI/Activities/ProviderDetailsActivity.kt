package com.aait.mughasilclient.UI.Activities

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.app.ActivityCompat
import com.aait.mughasilclient.Base.Parent_Activity
import com.aait.mughasilclient.Client
import com.aait.mughasilclient.Models.ConversationIdResponse
import com.aait.mughasilclient.Models.ProviderDetailsResponse
import com.aait.mughasilclient.Models.SwitchNotification
import com.aait.mughasilclient.Network.Service
import com.aait.mughasilclient.R
import com.aait.mughasilclient.Uitls.CommonUtil
import com.aait.mughasilclient.Uitls.PermissionUtils
import com.bumptech.glide.Glide
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ProviderDetailsActivity:Parent_Activity() {
    override val layoutResource: Int
        get() = R.layout.activity_provider_details
    lateinit var back:ImageView
    lateinit var name:TextView
    lateinit var address:TextView
//    lateinit var calling:LinearLayout
//    lateinit var chat:LinearLayout
    lateinit var order:Button
    lateinit var title:TextView
    var id = 0
    var phone = ""
    var image  = ""
    lateinit var photo:ImageView

    override fun initializeComponents() {
        id = intent.getIntExtra("id",0)
        phone = intent.getStringExtra("phone")
        image = intent.getStringExtra("image")
        back = findViewById(R.id.back)
        name = findViewById(R.id.name)
        address = findViewById(R.id.address)
        photo = findViewById(R.id.image)
        title = findViewById(R.id.title)
        title.text = getString(R.string.Laundry_information)
//        calling = findViewById(R.id.call)
//        chat = findViewById(R.id.chat)
        order = findViewById(R.id.order)
        back.setOnClickListener { startActivity(Intent(this,MainActivity::class.java)) }
//        calling.setOnClickListener { getLocationWithPermission(phone) }
        Glide.with(mContext).load(image).into(photo)
        getProvider()
       // switchPhone()
        order.setOnClickListener { val intent = Intent(this,OrderLocationActivity::class.java)
        intent.putExtra("id",id)
        intent.putExtra("phone",phone)
        startActivity(intent)}
//        chat.setOnClickListener { getID() }

    }
    internal fun callnumber(number: String) {
        val intent = Intent(Intent.ACTION_DIAL)
        intent.data = Uri.parse("tel:$number")
        mContext.startActivity(intent)
    }

    fun getLocationWithPermission(number: String) {
        if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
            if (!PermissionUtils.hasPermissions(mContext, Manifest.permission.CALL_PHONE)) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
                    ActivityCompat.requestPermissions(
                        mContext as Activity, PermissionUtils.CALL_PHONE,
                        300
                    )
            } else {
                callnumber(number)
            }
        } else {
            callnumber(number)
        }

    }
    fun getProvider(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.providerDetails(id)?.enqueue(object:
            Callback<ProviderDetailsResponse>{
            override fun onFailure(call: Call<ProviderDetailsResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(
                call: Call<ProviderDetailsResponse>,
                response: Response<ProviderDetailsResponse>
            ) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        name.text = response.body()?.data?.name
                        address.text = response.body()?.data?.address
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }
        })
    }

    fun getID(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.ConversationID(mSharedPrefManager.userData.id!!,id)?.enqueue(object :
            Callback<ConversationIdResponse>{
            override fun onFailure(call: Call<ConversationIdResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(
                call: Call<ConversationIdResponse>,
                response: Response<ConversationIdResponse>
            ) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        val intent = Intent(this@ProviderDetailsActivity,ChatActivity::class.java)
                        intent.putExtra("id",response.body()?.data?.conversation_id)
                        intent.putExtra("receiver",response.body()?.data?.receiver_id)
                        intent.putExtra("lastpage",response.body()?.data?.lastPage)
                        startActivity(intent)
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }
        })
    }
//    fun switchPhone(){
//
//        Client.getClient()?.create(Service::class.java)?.switchPhone(mSharedPrefManager.userData.id!!,null)?.enqueue(object :
//            Callback<SwitchNotification> {
//            override fun onFailure(call: Call<SwitchNotification>, t: Throwable) {
//                CommonUtil.handleException(mContext,t)
//                t.printStackTrace()
//                hideProgressDialog()
//            }
//
//            override fun onResponse(
//                call: Call<SwitchNotification>,
//                response: Response<SwitchNotification>
//            ) {
//                hideProgressDialog()
//                if (response.isSuccessful){
//                    if (response.body()?.value.equals("1")){
//                        if (response.body()?.data==1){
//                            calling.visibility = View.VISIBLE
//                        }else{
//                            calling.visibility = View.GONE
//                        }
//
//                    }else{
//                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
//                    }
//                }
//            }
//
//        })
//    }
}