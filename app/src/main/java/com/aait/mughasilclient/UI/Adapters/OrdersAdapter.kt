package com.aait.mughasilclient.UI.Adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RatingBar
import android.widget.TextView
import com.aait.mughasilclient.Base.ParentRecyclerAdapter
import com.aait.mughasilclient.Base.ParentRecyclerViewHolder
import com.aait.mughasilclient.Models.OrderModel
import com.aait.mughasilclient.R

import com.bumptech.glide.Glide

class OrdersAdapter (context: Context, data: MutableList<OrderModel>, layoutId: Int) :
    ParentRecyclerAdapter<OrderModel>(context, data, layoutId) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ParentRecyclerViewHolder {
        val itemView = LayoutInflater.from(mcontext).inflate(layoutId, parent, false)
        return ViewHolder(itemView)
    }
    var TextView.textColor: Int
        get() = currentTextColor
        set(v) = setTextColor(v)

    override fun onBindViewHolder(holder: ParentRecyclerViewHolder, position: Int) {
        val viewHolder = holder as ViewHolder
        val listModel = data.get(position)
        viewHolder.name!!.setText(listModel.name)
        viewHolder.address!!.text = listModel.address!!
      //  Glide.with(mcontext).load(listModel.image!!).into(viewHolder.photo)
        Glide.with(mcontext).load(listModel.avatar).into(viewHolder.photo)
        viewHolder.order_num.text = mcontext.getString(R.string.order_num)+" : "+listModel.id!!.toString()
        viewHolder.itemView.setOnClickListener(View.OnClickListener { view -> onItemClickListener.onItemClick(view,position) })



    }
    inner class ViewHolder internal constructor(itemView: View) :
        ParentRecyclerViewHolder(itemView) {

        internal var photo=itemView.findViewById<ImageView>(R.id.Image)
        internal var name = itemView.findViewById<TextView>(R.id.name)
        internal var address = itemView.findViewById<TextView>(R.id.address)
        internal var order_num = itemView.findViewById<TextView>(R.id.order_num)

    }
}