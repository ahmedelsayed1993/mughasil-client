package com.aait.mughasilclient.UI.Activities

import android.content.Intent
import android.util.Log
import android.view.Gravity
import android.view.View
import android.widget.*
import androidx.core.view.GravityCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.aait.mughasilclient.Base.Parent_Activity
import com.aait.mughasilclient.Client
import com.aait.mughasilclient.Listeners.OnItemClickListener
import com.aait.mughasilclient.Models.*
import com.aait.mughasilclient.Network.Service
import com.aait.mughasilclient.R
import com.aait.mughasilclient.UI.Adapters.ConversationsAdapter
import com.aait.mughasilclient.UI.Adapters.NotificationAdapter
import com.aait.mughasilclient.Uitls.CommonUtil
import com.bumptech.glide.Glide
import com.google.firebase.iid.FirebaseInstanceId
import com.google.gson.Gson
import de.hdodenhof.circleimageview.CircleImageView
import kotlinx.android.synthetic.main.activity_main.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class NotificationActivity:Parent_Activity(),OnItemClickListener {
    override fun onItemClick(view: View, position: Int) {
        if (view.id == R.id.delete) {
            showProgressDialog(getString(R.string.please_wait))
            Client.getClient()?.create(Service::class.java)?.delete(
                 mSharedPrefManager.userData.id!!
                , notificationModels.get(position).id!!
            )?.enqueue(object :
                Callback<AboutAppResponse> {
                override fun onFailure(call: Call<AboutAppResponse>, t: Throwable) {
                    CommonUtil.handleException(mContext, t)
                    t.printStackTrace()
                    hideProgressDialog()
                }

                override fun onResponse(
                    call: Call<AboutAppResponse>,
                    response: Response<AboutAppResponse>
                ) {
                    hideProgressDialog()
                    if (response.isSuccessful) {
                        if (response.body()?.value.equals("1")) {
                            CommonUtil.makeToast(mContext, response.body()?.data!!)
                            getHome()
                        } else {
                            CommonUtil.makeToast(mContext, response.body()?.msg!!)
                        }
                    }
                }

            })
        }

    }

    override val layoutResource: Int
        get() = R.layout.activity_notifications
    lateinit var about_app: LinearLayout
    lateinit var terms: LinearLayout
    lateinit var call_us: LinearLayout
    lateinit var settings: LinearLayout
    lateinit var chats: LinearLayout
    lateinit var orders: LinearLayout
    lateinit var profile: LinearLayout
    lateinit var notification: LinearLayout
    lateinit var image: CircleImageView
    lateinit var name:TextView
    lateinit var address:TextView
    lateinit var menu:ImageView
    lateinit var title:TextView
    lateinit var back:ImageView
    lateinit var home:LinearLayout
    lateinit var logout: Button
   // lateinit var load:ImageView
    var deviceID = ""
    lateinit var rv_recycle: RecyclerView
    internal var layNoInternet: RelativeLayout? = null
    internal var layNoItem: RelativeLayout? = null
    internal var tvNoContent: TextView? = null
    var swipeRefresh: SwipeRefreshLayout? = null
    internal lateinit var linearLayoutManager: LinearLayoutManager
    internal var notificationModels =  java.util.ArrayList<NotificationModel>()
    internal lateinit var notificationAdapter: NotificationAdapter

    override fun initializeComponents() {
        deviceID = FirebaseInstanceId.getInstance().token.toString()
        menu = findViewById(R.id.menu)
        title = findViewById(R.id.title)
        back = findViewById(R.id.back)
        menu.setOnClickListener { startActivity(Intent(this,MainActivity::class.java))
        finish()}
        back.visibility = View.GONE
        title.text = getString(R.string.notification)
       // sideMenu()

        rv_recycle = findViewById(R.id.rv_recycle)
        layNoInternet = findViewById(R.id.lay_no_internet)
        layNoItem = findViewById(R.id.lay_no_item)
        tvNoContent = findViewById(R.id.tv_no_content)
        swipeRefresh = findViewById(R.id.swipe_refresh)
        linearLayoutManager = LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false)
        notificationAdapter =  NotificationAdapter(mContext,notificationModels,R.layout.recycler_notifications)
        notificationAdapter.setOnItemClickListener(this)
        rv_recycle!!.layoutManager= linearLayoutManager
        rv_recycle!!.adapter = notificationAdapter
        swipeRefresh!!.setColorSchemeResources(
            R.color.colorPrimary,
            R.color.colorPrimaryDark,
            R.color.colorAccent
        )

        swipeRefresh!!.setOnRefreshListener { getHome() }
        getHome()
    }

    fun getHome(){
        showProgressDialog(getString(R.string.please_wait))
        layNoInternet!!.visibility = View.GONE
        layNoItem!!.visibility = View.GONE
        Client.getClient()?.create(Service::class.java)?.Notifications(mSharedPrefManager.userData.id!!)?.enqueue(object :
            Callback<NotificationResponse> {
            override fun onResponse(call: Call<NotificationResponse>, response: Response<NotificationResponse>) {
                swipeRefresh!!.isRefreshing = false
                hideProgressDialog()
                if (response.isSuccessful) {
                    try {
                        if (response.body()?.value.equals("1")) {
                            Log.e("myJobs", Gson().toJson(response.body()!!.data))
                            if (response.body()!!.data?.isEmpty()!!) {
                                layNoItem!!.visibility = View.VISIBLE
                                layNoInternet!!.visibility = View.GONE
                                tvNoContent!!.setText(R.string.content_not_found_you_can_still_search_the_app_freely)
                            } else {
//                            initSliderAds(response.body()?.slider!!)
                                notificationAdapter.updateAll(response.body()!!.data!!)
                            }
//                            response.body()!!.data?.let { homeSeekerAdapter.updateAll(it)
                        }else {
                            CommonUtil.makeToast(mContext,response.body()?.msg!!)
                        }

                    } catch (e: Exception) {e.printStackTrace() }

                } else {  }
            }
            override fun onFailure(call: Call<NotificationResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                layNoInternet!!.visibility = View.VISIBLE
                layNoItem!!.visibility = View.GONE
                swipeRefresh!!.isRefreshing = false
                hideProgressDialog()
                Log.e("response", Gson().toJson(t))
            }
        })


    }
//    fun sideMenu(){
//        drawer_layout.useCustomBehavior(Gravity.START)
//        drawer_layout.useCustomBehavior(Gravity.END)
//        drawer_layout.setRadius(Gravity.START, 100f)
//        drawer_layout.setRadius(Gravity.END, 100f)
//        drawer_layout.setViewScale(Gravity.START, 0.95f) //set height scale for main view (0f to 1f)
//        drawer_layout.setViewScale(Gravity.END, 0.95f) //set height scale for main view (0f to 1f)
//        drawer_layout.setViewElevation(Gravity.START, 0f) //set main view elevation when drawer open (dimension)
//        drawer_layout.setViewElevation(Gravity.END, 0f) //set main view elevation when drawer open (dimension)
////        drawer_layout.setViewScrimColor(Gravity.START, Color.) //set drawer overlay coloe (color)
////        drawer_layout.setViewScrimColor(Gravity.END, Color.TRANSPARENT) //set drawer overlay coloe (color)
//        drawer_layout.setDrawerElevation(Gravity.START, 100f) //set drawer elevation (dimension)
//        drawer_layout.setDrawerElevation(Gravity.END, 100f) //set drawer elevation (dimension)
//        drawer_layout.setContrastThreshold(3f) //set maximum of contrast ratio between white text and background color.
//        drawer_layout.setRadius(Gravity.START, 0f) //set end container's corner radius (dimension)
//
//        drawer_layout.setRadius(Gravity.END, 0f)
//        about_app = drawer_layout.findViewById(R.id.about_app)
//        about_app.setOnClickListener { startActivity(Intent(this@NotificationActivity,AboutAppActivity::class.java)) }
//        terms = drawer_layout.findViewById(R.id.terms)
//        terms.setOnClickListener { startActivity(Intent(this,TermsActivity::class.java)) }
//        call_us = drawer_layout.findViewById(R.id.call_us)
//        call_us.setOnClickListener { startActivity(Intent(this,CallUsActivity::class.java))
//        }
//        settings = drawer_layout.findViewById(R.id.settings)
//        settings.setOnClickListener { startActivity(Intent(this,SettingsActivity::class.java)) }
//        chats = drawer_layout.findViewById(R.id.chats)
//        chats.setOnClickListener { startActivity(Intent(this,ChatsActivity::class.java)) }
//        profile = drawer_layout.findViewById(R.id.profile)
//        profile.setOnClickListener { startActivity(Intent(this,ProfileActivity::class.java)) }
////        notification = drawer_layout.findViewById(R.id.notifications)
////        notification.setOnClickListener { drawer_layout.closeDrawer(GravityCompat.START)
////        }
//        image = drawer_layout.findViewById(R.id.image)
//        name = drawer_layout.findViewById(R.id.name)
//        address = drawer_layout.findViewById(R.id.address)
//        Glide.with(mContext).asBitmap().load(mSharedPrefManager.userData.avatar).into(image)
//        name.text = mSharedPrefManager.userData.name!!
//        address.text = mSharedPrefManager.userData.email!!
//        menu.setOnClickListener { drawer_layout.openDrawer(GravityCompat.START) }
//        orders = drawer_layout.findViewById(R.id.orders)
//        orders.setOnClickListener { startActivity(Intent(this,OrdersActivity::class.java)) }
//        home = drawer_layout.findViewById(R.id.home)
//        home.setOnClickListener { startActivity(Intent(this,MainActivity::class.java)) }
//        logout = drawer_layout.findViewById(R.id.logout)
//        logout.setOnClickListener { logout() }
//    }
//
//    fun logout(){
//        showProgressDialog(getString(R.string.please_wait))
//        Client.getClient()?.create(Service::class.java)?.logOut(mSharedPrefManager.userData.id!!,deviceID,"android",mLanguagePrefManager.appLanguage)?.enqueue(object :
//            Callback<AboutAppResponse> {
//            override fun onFailure(call: Call<AboutAppResponse>, t: Throwable) {
//                CommonUtil.handleException(mContext,t)
//                t.printStackTrace()
//                hideProgressDialog()
//            }
//
//            override fun onResponse(
//                call: Call<AboutAppResponse>,
//                response: Response<AboutAppResponse>
//            ) {
//                hideProgressDialog()
//                if (response.isSuccessful){
//                    if (response.body()?.value.equals("1")){
//                        mSharedPrefManager.loginStatus=false
//                        mSharedPrefManager.Logout()
//                        CommonUtil.makeToast(applicationContext,response.body()?.data!!)
//                        startActivity(Intent(this@NotificationActivity, SplashActivity::class.java))
//                        finish()
//                    }else{
//                        CommonUtil.makeToast(applicationContext,response.body()?.msg!!)
//
//                    }
//                }
//            }
//        })
//    }
}