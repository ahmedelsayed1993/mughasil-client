package com.aait.mughasilclient.UI.Adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.aait.mughasilclient.Base.ParentRecyclerAdapter
import com.aait.mughasilclient.Base.ParentRecyclerViewHolder
import com.aait.mughasilclient.Models.NotificationModel
import com.aait.mughasilclient.Models.ProviderModel
import com.aait.mughasilclient.R
import com.bumptech.glide.Glide

class ProvidersAdapter (context: Context, data: MutableList<ProviderModel>, layoutId: Int) :
    ParentRecyclerAdapter<ProviderModel>(context, data, layoutId) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ParentRecyclerViewHolder {
        val itemView = LayoutInflater.from(mcontext).inflate(layoutId, parent, false)
        return ViewHolder(itemView)
    }
    var TextView.textColor: Int
        get() = currentTextColor
        set(v) = setTextColor(v)

    override fun onBindViewHolder(holder: ParentRecyclerViewHolder, position: Int) {
        val viewHolder = holder as ViewHolder
        val providerModel = data.get(position)
        viewHolder.name!!.setText(providerModel.name)
        viewHolder.distane!!.text = providerModel.distance!!.toString()
        viewHolder.address.text = providerModel.address
        Glide.with(mcontext).asBitmap().load(providerModel.avatar).into(viewHolder.image)

        viewHolder.itemView.setOnClickListener(View.OnClickListener { view -> onItemClickListener.onItemClick(view,position) })



    }
    inner class ViewHolder internal constructor(itemView: View) :
        ParentRecyclerViewHolder(itemView) {






        internal var image = itemView.findViewById<ImageView>(R.id.image)
        internal var name = itemView.findViewById<TextView>(R.id.name)
        internal var distane = itemView.findViewById<TextView>(R.id.distance)
        internal var address = itemView.findViewById<TextView>(R.id.address)



    }
}