package com.aait.mughasilclient.UI.Activities

import android.annotation.SuppressLint
import android.annotation.TargetApi
import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.content.Intent
import android.os.Build
import android.util.Log
import android.view.View
import android.widget.*
import androidx.annotation.RequiresApi
import com.aait.mughasilclient.Base.Parent_Activity
import com.aait.mughasilclient.Client
import com.aait.mughasilclient.Models.AboutAppResponse
import com.aait.mughasilclient.Models.EditOrderResponse
import com.aait.mughasilclient.Network.Service
import com.aait.mughasilclient.R
import com.aait.mughasilclient.Uitls.CommonUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.util.*

class EditOrderActivity:Parent_Activity() {
    override val layoutResource: Int
        get() = R.layout.activity_add_order
    var id = 0

    lateinit var another_location:TextView
    lateinit var phone:EditText
    lateinit var from:TextView
    lateinit var to:TextView
    lateinit var date:TextView
    lateinit var confirm:Button
    lateinit var title:TextView
    lateinit var back:ImageView

    var starting = 0
    var ending = 0
    var address = ""
    var lat = ""
    var lng = ""
    var number = ""
    lateinit var now:TextView
    lateinit var another_time:TextView
    lateinit var text:TextView
    lateinit var from_lay:LinearLayout
    lateinit var to_lay:LinearLayout
    lateinit var date_lay:LinearLayout
    var time = "now"

    @TargetApi(Build.VERSION_CODES.O)
    @RequiresApi(Build.VERSION_CODES.O)
    @SuppressLint("SetTextI18n")
    override fun initializeComponents() {
        id = intent.getIntExtra("id",0)


        another_location = findViewById(R.id.another_location)

        phone = findViewById(R.id.phone)
        from = findViewById(R.id.from)
        to = findViewById(R.id.to)
        date = findViewById(R.id.date)
        from = findViewById(R.id.from)
        title = findViewById(R.id.title)
        back = findViewById(R.id.back)
        now = findViewById(R.id.now)
        another_time = findViewById(R.id.another_time)
        text = findViewById(R.id.text)
        from_lay = findViewById(R.id.from_lay)
        to_lay = findViewById(R.id.to_lay)
        date_lay = findViewById(R.id.date_lay)
        confirm = findViewById(R.id.confirm)


        to_lay.visibility = View.GONE
        from_lay.visibility = View.GONE

        val current = LocalDateTime.now()

        val formatter = DateTimeFormatter.ofPattern("yyyy/MM/dd")
        val formatter1 = DateTimeFormatter.ofPattern("HH:mm")
        Log.e("date",current.format(formatter).toString())
        Log.e("time",current.format(formatter1).toString())
        now.setOnClickListener {
            time = "now"
            now.setBackgroundColor(mContext.resources.getColor(R.color.color_))
            another_time.setBackgroundColor(mContext.resources.getColor(R.color.transparent))
            to_lay.visibility = View.GONE
            from_lay.visibility = View.GONE
        }
        another_time.setOnClickListener {
            time = "another"
            another_time.setBackgroundColor(mContext.resources.getColor(R.color.color_))
            now.setBackgroundColor(mContext.resources.getColor(R.color.transparent))
            to_lay.visibility = View.VISIBLE
            from_lay.visibility = View.VISIBLE
        }

        title.text = getString(R.string.detect_location)
        back.setOnClickListener { onBackPressed()
            finish()}
        another_location.setOnClickListener { startActivityForResult(Intent(this@EditOrderActivity,LocationActivity::class.java),1)  }
        from.setOnClickListener {
            val myCalender = Calendar.getInstance()
            val hour = myCalender.get(Calendar.HOUR_OF_DAY)
            val minute = myCalender.get(Calendar.MINUTE)


            val myTimeListener =
                TimePickerDialog.OnTimeSetListener { view, hourOfDay, minute ->
                    if (view.isShown) {
                        myCalender.set(Calendar.HOUR_OF_DAY, hourOfDay)
                        myCalender.set(Calendar.MINUTE, minute)
                        starting = hourOfDay
                        var hour = hourOfDay
                        var am_pm = ""
                        // AM_PM decider logic
                        when {
                            hour == 0 -> {
                                hour += 12
                                am_pm = "AM"
                            }
                            hour == 12 -> am_pm = "PM"
                            hour > 12 -> {
                                hour -= 12
                                am_pm = "PM"
                            }
                            else -> am_pm = "AM"
                        }

                        val hours = if (hour < 10) "0" + hour else hour
                        from.text = " $hours $am_pm"

                    }
                }
            val timePickerDialog = TimePickerDialog(
                this,
                android.R.style.Theme_Holo_Light_Dialog_NoActionBar,
                myTimeListener,
                hour,
                minute,
                false
            )
            timePickerDialog.setTitle(getString(R.string.from))
            timePickerDialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
            timePickerDialog.show()
        }
        to.setOnClickListener {
            val myCalender = Calendar.getInstance()
            val hour = myCalender.get(Calendar.HOUR_OF_DAY)
            val minute = myCalender.get(Calendar.MINUTE)


            val myTimeListener =
                TimePickerDialog.OnTimeSetListener { view, hourOfDay, minute ->
                    if (view.isShown) {
                        myCalender.set(Calendar.HOUR_OF_DAY, hourOfDay)
                        myCalender.set(Calendar.MINUTE, minute)
                        ending = hourOfDay
                        var hour = hourOfDay
                        var am_pm = ""
                        // AM_PM decider logic
                        when {
                            hour == 0 -> {
                                hour += 12
                                am_pm = "AM"
                            }
                            hour == 12 -> am_pm = "PM"
                            hour > 12 -> {
                                hour -= 12
                                am_pm = "PM"
                            }
                            else -> am_pm = "AM"
                        }

                        val hours = if (hour < 10) "0" + hour else hour
                        to.text = " $hours $am_pm"

                    }
                }
            val timePickerDialog = TimePickerDialog(
                this,
                android.R.style.Theme_Holo_Light_Dialog_NoActionBar,
                myTimeListener,
                hour,
                minute,
                false
            )
            timePickerDialog.setTitle(getString(R.string.to))
            timePickerDialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
            timePickerDialog.show()
        }

        date.setOnClickListener {
            val c = Calendar.getInstance()
            val year = c.get(Calendar.YEAR)
            val month = c.get(Calendar.MONTH)
            val day = c.get(Calendar.DAY_OF_MONTH)




            val dpd = DatePickerDialog(mContext, DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->

                // Display Selected date in textbox
                var m = monthOfYear+1
                date.setText("" + dayOfMonth + "/" + m + "/" + year)

            }, year, month, day)
            dpd.datePicker.minDate = System.currentTimeMillis() - 1000

            dpd.show()
        }


        confirm.setOnClickListener {

                if (CommonUtil.checkEditError(phone, getString(R.string.phone_number))||
                    CommonUtil.checkTextError(another_location,getString(R.string.location)) ||
                    CommonUtil.checkTextError(date, getString(R.string.date))
                        ) {
                    return@setOnClickListener
                } else {
                    if (time.equals("now")) {
                        AddOrder(current.format(formatter1).toString(), current.format(formatter1).toString(), date.text.toString())
                    } else {
                        if (CommonUtil.checkTextError(from, getString(R.string.from)) ||
                            CommonUtil.checkTextError(to, getString(R.string.to))
                        ) {
                            return@setOnClickListener
                        } else {
                            if (ending < starting) {
                                to.error = "وقت النهاية يجب ان يكون اكبر من وقت البداية"
                            } else {
                                AddOrder(
                                    from.text.toString(),
                                    to.text.toString(),
                                    date.text.toString()
                                )
                            }
                        }
                    }

                }
        }

         getOrder()


    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        Log.e("address",data?.getStringExtra("result").toString())
        if (data?.getStringExtra("result")!=null) {
            address = data?.getStringExtra("result").toString()
            lat = data?.getStringExtra("lat").toString()
            lng = data?.getStringExtra("lng").toString()
            another_location.text = address

        }else{
            address = mSharedPrefManager.userData.address!!
            lat = mSharedPrefManager.userData.lat!!
            lng = mSharedPrefManager.userData.lng!!
            another_location.text = ""

        }
    }

    fun AddOrder(time:String,to:String,date:String){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.EditOrder(mSharedPrefManager.userData.id!!,id,address,lat,lng,phone.text.toString(),time,to,date)?.enqueue(object :
            Callback<EditOrderResponse> {
            override fun onFailure(call: Call<EditOrderResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(
                call: Call<EditOrderResponse>,
                response: Response<EditOrderResponse>
            ) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                        val intent = Intent(this@EditOrderActivity,MainActivity::class.java)

                        startActivity(intent)
                        finish()
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }
        })
    }
    fun getOrder(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.EditOrder(mSharedPrefManager.userData.id!!,id,null,null,null,null,null,null,null)?.enqueue(object :
            Callback<EditOrderResponse> {
            override fun onFailure(call: Call<EditOrderResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(
                call: Call<EditOrderResponse>,
                response: Response<EditOrderResponse>
            ) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                       lat = response.body()?.data?.lat!!
                        lng = response.body()?.data?.lng!!
                        address = response.body()?.data?.address!!
                        another_location.text = address
                        phone.setText(response.body()?.data?.phone!!)
                        date.text = response.body()?.data?.date
                        if (response.body()?.data?.time_from.equals("now")){
                            time = "now"
                            now.setBackgroundColor(mContext.resources.getColor(R.color.color_))
                            another_time.setBackgroundColor(mContext.resources.getColor(R.color.transparent))
                            to_lay.visibility = View.GONE
                            from_lay.visibility = View.GONE
                        }else{
                            time = "another"
                            another_time.setBackgroundColor(mContext.resources.getColor(R.color.color_))
                            now.setBackgroundColor(mContext.resources.getColor(R.color.transparent))
                            to_lay.visibility = View.VISIBLE
                            from_lay.visibility = View.VISIBLE
                            to.text = response.body()?.data?.time_to
                            from.text = response.body()?.data?.time_from
                        }

                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }
        })
    }

}