package com.aait.mughasilclient.UI.Activities

import android.Manifest
import android.content.DialogInterface
import android.content.Intent
import android.location.Address
import android.location.Geocoder
import android.os.Build
import android.provider.Settings
import android.util.Log
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import com.aait.mughasilclient.Base.Parent_Activity
import com.aait.mughasilclient.GPS.GPSTracker
import com.aait.mughasilclient.GPS.GpsTrakerListener
import com.aait.mughasilclient.R
import com.aait.mughasilclient.Uitls.CommonUtil
import com.aait.mughasilclient.Uitls.DialogUtil
import com.aait.mughasilclient.Uitls.PermissionUtils
import com.google.android.gms.maps.*
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import java.io.IOException
import java.util.*

class LocationActivity:Parent_Activity() , OnMapReadyCallback, GoogleMap.OnMapClickListener,
    GpsTrakerListener {

    lateinit var map:MapView
    lateinit var location:TextView
    lateinit var confirm:Button
    internal lateinit var googleMap: GoogleMap
    internal lateinit var myMarker: Marker
    internal lateinit var geocoder: Geocoder
    internal lateinit var gps: GPSTracker
    internal var startTracker = false
    var mLang = ""
    var mLat = ""
    var result = ""
    private var mAlertDialog: AlertDialog? = null

    override fun onMapReady(p0: GoogleMap?) {
        this.googleMap = p0!!
        googleMap.setOnMapClickListener(this)
        getLocationWithPermission()
    }

    override fun onMapClick(p0: LatLng?) {
        Log.e("LatLng", p0.toString())
        mLang =  java.lang.Double.toString(p0?.latitude!!) .toString()
        mLat =  java.lang.Double.toString(p0?.longitude!!) .toString()
        if (myMarker != null) {
            myMarker.remove()
            putMapMarker(p0?.latitude, p0?.longitude)
        } else {
            putMapMarker(p0?.latitude, p0?.longitude)
        }

        if (p0?.latitude != 0.0 && p0?.longitude != 0.0) {
            putMapMarker(p0?.latitude, p0?.longitude)
            mLat = p0?.latitude.toString()
            mLang = p0?.longitude.toString()
            val addresses: List<Address>

            geocoder = Geocoder(this@LocationActivity, Locale.getDefault())

            try {
                addresses = geocoder.getFromLocation(
                    java.lang.Double.parseDouble(mLat),
                    java.lang.Double.parseDouble(mLang),
                    1
                )

                if (addresses.isEmpty()) {
                    Toast.makeText(
                        this@LocationActivity,
                        resources.getString(R.string.detect_location),
                        Toast.LENGTH_SHORT
                    ).show()
                } else {
                    result = addresses[0].getAddressLine(0)
                    Log.e("address",result)
                    location.text = addresses[0].getAddressLine(0)
                    CommonUtil.makeToast(mContext,addresses[0].getAddressLine(0))
                }


            } catch (e: IOException) {
            }

            googleMap.clear()
            putMapMarker(p0?.latitude, p0?.longitude)
        }
    }

    override fun onTrackerSuccess(lat: Double?, log: Double?) {
        if (startTracker) {
            if (lat != 0.0 && log != 0.0) {
                hideProgressDialog()
                putMapMarker(lat, log)
            }
        }
    }

    override fun onStartTracker() {
        startTracker = true
    }

    override val layoutResource: Int
        get() = R.layout.activity_location


    override fun initializeComponents() {
        map = findViewById(R.id.map)
        location = findViewById(R.id.location)
        confirm = findViewById(R.id.confirm)
        map.onCreate(mSavedInstanceState)
        map.onResume()
        map.getMapAsync(this)

        try {
            MapsInitializer.initialize(mContext)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        confirm.setOnClickListener {
            if (!result.equals("")) {
                val returnIntent = Intent()
                returnIntent.putExtra("result", result)
                returnIntent.putExtra("lat", mLat)
                returnIntent.putExtra("lng", mLang)
                setResult(1, returnIntent)
                finish()
            }else{

            }
        }

    }

    override fun onBackPressed() {
        super.onBackPressed()
        if (!result.equals("")) {
            val returnIntent = Intent()
            returnIntent.putExtra("result", result)
            returnIntent.putExtra("lat", mLat)
            returnIntent.putExtra("lng", mLang)
            setResult(1, returnIntent)
            finish()
        }else{
            val returnIntent = Intent()
            returnIntent.putExtra("result", "")
            returnIntent.putExtra("lat", "")
            returnIntent.putExtra("lng", "")
            setResult(1, returnIntent)
            finish()
        }
    }

    fun putMapMarker(lat: Double?, log: Double?) {
        val latLng = LatLng(lat!!, log!!)
        myMarker = googleMap.addMarker(
            MarkerOptions()
                .position(latLng)
                .title("موقعي")
                .icon(BitmapDescriptorFactory.fromResource(R.mipmap.maps))
        )
        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 10f))
        // kkjgj

    }

    fun getLocationWithPermission() {
        gps = GPSTracker(mContext, this)
        if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
            if (!(PermissionUtils.hasPermissions(mContext, Manifest.permission.ACCESS_FINE_LOCATION)&&
                        (PermissionUtils.hasPermissions(mContext,
                            Manifest.permission.ACCESS_COARSE_LOCATION)))) {
                CommonUtil.PrintLogE("Permission not granted")
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(
                        PermissionUtils.GPS_PERMISSION,
                        800
                    )
                    Log.e("GPS", "1")
                }
            } else {
                getCurrentLocation()
                Log.e("GPS", "2")
            }
        } else {
            Log.e("GPS", "3")
            getCurrentLocation()
        }

    }

    internal fun getCurrentLocation() {
        gps.getLocation()
        if (!gps.canGetLocation()) {
            mAlertDialog = DialogUtil.showAlertDialog(mContext,
                getString(R.string.gps_detecting),
                DialogInterface.OnClickListener { dialogInterface, i ->
                    mAlertDialog?.dismiss()
                    val intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                    startActivityForResult(intent, 300)
                })
        } else {
            if (gps.getLatitude() != 0.0 && gps.getLongitude() != 0.0) {
                putMapMarker(gps.getLatitude(), gps.getLongitude())
                mLat = gps.getLatitude().toString()
                mLang = gps.getLongitude().toString()
                val addresses: List<Address>
                geocoder = Geocoder(this, Locale.getDefault())
                try {
                    addresses = geocoder.getFromLocation(
                        java.lang.Double.parseDouble(mLat),
                        java.lang.Double.parseDouble(mLang),
                        1
                    )
                    if (addresses.isEmpty()) {
                        Toast.makeText(
                            mContext,
                            resources.getString(R.string.detect_location),
                            Toast.LENGTH_SHORT
                        ).show()
                    } else {
                        result = addresses[0].getAddressLine(0)
                        Log.e("address",result)
                        location.text = addresses[0].getAddressLine(0)
                        CommonUtil.makeToast(mContext,addresses[0].getAddressLine(0))



                    }

                } catch (e: IOException) {
                }

                googleMap.clear()
                putMapMarker(gps.getLatitude(), gps.getLongitude())

            }
        }
    }
}