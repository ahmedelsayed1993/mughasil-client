package com.aait.mughasilclient.UI.Activities

import android.content.Intent
import android.net.Uri
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import com.aait.mughasilclient.Base.Parent_Activity
import com.aait.mughasilclient.Client
import com.aait.mughasilclient.Models.ContactUsResponse
import com.aait.mughasilclient.Network.Service
import com.aait.mughasilclient.R
import com.aait.mughasilclient.Uitls.CommonUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class CallUsActivity :Parent_Activity(){
    override val layoutResource: Int
        get() = R.layout.activity_contact_us
    lateinit var name:EditText
    lateinit var mail:EditText
    lateinit var message:EditText
    lateinit var send:Button
    lateinit var face:ImageView
    lateinit var twitter:ImageView
    lateinit var insta:ImageView
    lateinit var back:ImageView
    lateinit var title:TextView
    lateinit var menu:ImageView
    var facebook = ""
    var twit = ""
    var instagram = ""

    override fun initializeComponents() {
        name = findViewById(R.id.name)
        mail = findViewById(R.id.mail)
        message = findViewById(R.id.message)
        send = findViewById(R.id.send)
        face = findViewById(R.id.face)
        twitter = findViewById(R.id.twitter)
        insta = findViewById(R.id.insta)
        back = findViewById(R.id.back)
        title = findViewById(R.id.title)
        menu = findViewById(R.id.menu)

        title.text = getString(R.string.call_us)
        back.visibility = View.GONE
        menu.setOnClickListener { startActivity(Intent(this,MainActivity::class.java))
            finish()}
        back.setOnClickListener { startActivity(Intent(this,NotificationActivity::class.java))
            finish()}

        getContact()
        face.setOnClickListener {
            if (!facebook.equals(""))
            {
                if (facebook.startsWith("http"))
                {
                    Log.e("here", "333")
                    val i = Intent(Intent.ACTION_VIEW)
                    i.data = Uri.parse(facebook)
                    startActivity(i)

                } else {
                    Log.e("here", "4444")
                    val url = "https://$facebook"
                    val i = Intent(Intent.ACTION_VIEW)
                    i.data = Uri.parse(url)
                    startActivity(i)
                }
            }
        }
        twitter.setOnClickListener {
            if (!twit.equals(""))
            {
                if (twit.startsWith("http"))
                {
                    Log.e("here", "333")
                    val i = Intent(Intent.ACTION_VIEW)
                    i.data = Uri.parse(twit)
                    startActivity(i)

                } else {
                    Log.e("here", "4444")
                    val url = "https://$twit"
                    val i = Intent(Intent.ACTION_VIEW)
                    i.data = Uri.parse(url)
                    startActivity(i)
                }
            }
        }
        insta.setOnClickListener {
            if (!instagram.equals(""))
            {
                if (instagram.startsWith("http"))
                {
                    Log.e("here", "333")
                    val i = Intent(Intent.ACTION_VIEW)
                    i.data = Uri.parse(instagram)
                    startActivity(i)

                } else {
                    Log.e("here", "4444")
                    val url = "https://$instagram"
                    val i = Intent(Intent.ACTION_VIEW)
                    i.data = Uri.parse(url)
                    startActivity(i)
                }
            }
        }

        send.setOnClickListener {
            if (CommonUtil.checkEditError(name,getString(R.string.name))||
                CommonUtil.checkEditError(mail,getString(R.string.email))||
                !CommonUtil.isEmailValid(mail,getString(R.string.correct_email))||
                CommonUtil.checkEditError(message,getString(R.string.write_message))){
                return@setOnClickListener
            }else{
                Contact()
            }
        }
    }

    fun getContact(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.Contact(null,null,null)?.enqueue(object :
            Callback<ContactUsResponse> {
            override fun onFailure(call: Call<ContactUsResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(
                call: Call<ContactUsResponse>,
                response: Response<ContactUsResponse>
            ) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        facebook = response.body()?.data?.get(2)?.link!!
                        twit = response.body()?.data?.get(1)?.link!!
                        instagram = response.body()?.data?.get(0)?.link!!
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }
        })
    }

    fun Contact(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.Contact(name.text.toString(),mail.text.toString(),message.text.toString())?.enqueue(object :
            Callback<ContactUsResponse> {
            override fun onFailure(call: Call<ContactUsResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(
                call: Call<ContactUsResponse>,
                response: Response<ContactUsResponse>
            ) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                        facebook = response.body()?.data?.get(2)?.link!!
                        twit = response.body()?.data?.get(1)?.link!!
                        instagram = response.body()?.data?.get(0)?.link!!
                        startActivity(Intent(this@CallUsActivity,MainActivity::class.java))
                        finish()
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }
        })
    }
}