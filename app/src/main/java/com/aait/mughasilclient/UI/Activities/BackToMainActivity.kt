package com.aait.mughasilclient.UI.Activities

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.core.app.ActivityCompat
import com.aait.mughasilclient.Base.Parent_Activity
import com.aait.mughasilclient.Client
import com.aait.mughasilclient.Models.ConversationIdResponse
import com.aait.mughasilclient.Models.SwitchNotification
import com.aait.mughasilclient.Network.Service
import com.aait.mughasilclient.R
import com.aait.mughasilclient.Uitls.CommonUtil
import com.aait.mughasilclient.Uitls.PermissionUtils
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class BackToMainActivity:Parent_Activity() {
    override val layoutResource: Int
        get() = R.layout.activity_back_to_main
    lateinit var phone:ImageView
    lateinit var chat:LinearLayout
    lateinit var back:Button
    lateinit var calling:LinearLayout
    var number = ""
    var id = 0

    override fun initializeComponents() {
        number = intent.getStringExtra("number")
        id = intent.getIntExtra("id",0)
        phone = findViewById(R.id.phone)
        chat = findViewById(R.id.chat)
        back = findViewById(R.id.back)
        calling = findViewById(R.id.calling)
        back.setOnClickListener { startActivity(Intent(this,MainActivity::class.java))
        finish()}
       // switchPhone()
        calling.setOnClickListener { getLocationWithPermission(number) }
        chat.setOnClickListener { getID()}

    }

    override fun onBackPressed() {
        super.onBackPressed()
        startActivity(Intent(this,MainActivity::class.java))
        finish()
    }
    internal fun callnumber(number: String) {
        val intent = Intent(Intent.ACTION_DIAL)
        intent.data = Uri.parse("tel:$number")
        mContext.startActivity(intent)
    }

    fun getLocationWithPermission(number: String) {
        if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
            if (!PermissionUtils.hasPermissions(mContext, Manifest.permission.CALL_PHONE)) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
                    ActivityCompat.requestPermissions(
                        mContext as Activity, PermissionUtils.CALL_PHONE,
                        300
                    )
            } else {
                callnumber(number)
            }
        } else {
            callnumber(number)
        }

    }

    fun getID(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.ConversationID(mSharedPrefManager.userData.id!!,id)?.enqueue(object :
            Callback<ConversationIdResponse>{
            override fun onFailure(call: Call<ConversationIdResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(
                call: Call<ConversationIdResponse>,
                response: Response<ConversationIdResponse>
            ) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        val intent = Intent(this@BackToMainActivity,ChatActivity::class.java)
                        intent.putExtra("id",response.body()?.data?.conversation_id)
                        intent.putExtra("receiver",response.body()?.data?.receiver_id)
                        intent.putExtra("lastpage",response.body()?.data?.lastPage)
                        startActivity(intent)
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }
        })
    }
//    fun switchPhone(){
//        showProgressDialog(getString(R.string.please_wait))
//        Client.getClient()?.create(Service::class.java)?.switchPhone(mSharedPrefManager.userData.id!!,null)?.enqueue(object :
//            Callback<SwitchNotification> {
//            override fun onFailure(call: Call<SwitchNotification>, t: Throwable) {
//                CommonUtil.handleException(mContext,t)
//                t.printStackTrace()
//                hideProgressDialog()
//            }
//
//            override fun onResponse(
//                call: Call<SwitchNotification>,
//                response: Response<SwitchNotification>
//            ) {
//                hideProgressDialog()
//                if (response.isSuccessful){
//                    if (response.body()?.value.equals("1")){
//                        if (response.body()?.data==1){
//                            calling.visibility = View.VISIBLE
//                        }else{
//                            calling.visibility = View.GONE
//                        }
//
//                    }else{
//                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
//                    }
//                }
//            }
//
//        })
//    }
}