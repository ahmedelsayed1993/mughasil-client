package com.aait.mughasilclient.UI.Activities

import android.content.Intent
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.aait.mughasilclient.Base.Parent_Activity
import com.aait.mughasilclient.Client
import com.aait.mughasilclient.Models.AboutAppResponse
import com.aait.mughasilclient.Network.Service
import com.aait.mughasilclient.R
import com.aait.mughasilclient.Uitls.CommonUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class TermsActivity:Parent_Activity() {
    override val layoutResource: Int
        get() = R.layout.activity_about_app
    lateinit var back:ImageView
    lateinit var menu:ImageView
    lateinit var title:TextView
    lateinit var about:TextView
    var type = ""

    override fun initializeComponents() {
        type = intent.getStringExtra("type")
        about = findViewById(R.id.about)
        title = findViewById(R.id.title)
        title.text = getString(R.string.terms_conditions)
        menu = findViewById(R.id.menu)
        back = findViewById(R.id.back)
        back.visibility = View.GONE
        menu.setOnClickListener {
            if (type.equals("one")){
                onBackPressed()
                finish()
            }else {
                startActivity(Intent(this, MainActivity::class.java))
                finish()
            }
            }
        back.setOnClickListener { startActivity(Intent(this,NotificationActivity::class.java))
            finish()}
        getAbout()
    }
    fun getAbout(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.Terms()?.enqueue(object :
            Callback<AboutAppResponse> {
            override fun onFailure(call: Call<AboutAppResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(
                call: Call<AboutAppResponse>,
                response: Response<AboutAppResponse>
            ) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        about.text = response.body()?.data!!
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }
}