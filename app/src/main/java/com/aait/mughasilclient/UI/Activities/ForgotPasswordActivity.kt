package com.aait.mughasilclient.UI.Activities

import android.content.Intent
import android.widget.Button
import android.widget.EditText
import com.aait.mughasilclient.Base.Parent_Activity
import com.aait.mughasilclient.Client
import com.aait.mughasilclient.Models.UserResponse
import com.aait.mughasilclient.Network.Service
import com.aait.mughasilclient.R
import com.aait.mughasilclient.Uitls.CommonUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ForgotPasswordActivity:Parent_Activity() {
    override val layoutResource: Int
        get() = R.layout.activity_forgot_pass
    lateinit var phone:EditText
    lateinit var confirm:Button


    override fun initializeComponents() {
        phone = findViewById(R.id.phone)
        confirm = findViewById(R.id.confirm)
        phone.setSelection(phone.text.length)
        confirm.setOnClickListener {
            if (CommonUtil.checkEditError(phone,getString(R.string.phone_number))){
                return@setOnClickListener
            }else{
                ForgotPass()
            }
        }

    }
    fun ForgotPass(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.ForGot(phone.text.toString(),mLanguagePrefManager.appLanguage)?.enqueue(object :
            Callback<UserResponse>{
            override fun onFailure(call: Call<UserResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(call: Call<UserResponse>, response: Response<UserResponse>) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        if (response.body()?.data?.user_type.equals("user")) {
                            val intent =
                                Intent(this@ForgotPasswordActivity, NewPasswordActivity::class.java)
                            intent.putExtra("data", response.body()?.data)
                            startActivity(intent)
                            finish()
                        }else{
                            CommonUtil.makeToast(mContext,"هذا ليس حساب مستخدم")
                        }
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })

    }
}