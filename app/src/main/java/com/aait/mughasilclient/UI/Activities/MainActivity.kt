package com.aait.mughasilclient.UI.Activities

import android.Manifest
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.location.Address
import android.location.Geocoder
import android.net.Uri
import android.os.Build
import android.provider.Settings
import android.util.Log
import android.view.Gravity
import android.view.View
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.GravityCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.aait.mughasilclient.Base.Parent_Activity
import com.aait.mughasilclient.Client
import com.aait.mughasilclient.GPS.GPSTracker
import com.aait.mughasilclient.GPS.GpsTrakerListener
import com.aait.mughasilclient.Listeners.OnItemClickListener
import com.aait.mughasilclient.Models.*
import com.aait.mughasilclient.Network.Service
import com.aait.mughasilclient.R
import com.aait.mughasilclient.UI.Adapters.ConversationsAdapter
import com.aait.mughasilclient.UI.Adapters.NotificationAdapter
import com.aait.mughasilclient.UI.Adapters.ProvidersAdapter
import com.aait.mughasilclient.Uitls.CommonUtil
import com.aait.mughasilclient.Uitls.DialogUtil
import com.aait.mughasilclient.Uitls.PermissionUtils
import com.bumptech.glide.Glide
import com.google.firebase.iid.FirebaseInstanceId
import com.google.gson.Gson
import de.hdodenhof.circleimageview.CircleImageView
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_new_password.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException
import java.io.Serializable
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.util.*
import java.util.logging.SimpleFormatter

class MainActivity:Parent_Activity(),OnItemClickListener,GpsTrakerListener {
    override fun onTrackerSuccess(lat: Double?, log: Double?) {
        if (startTracker) {
            if (lat != 0.0 && log != 0.0) {
                hideProgressDialog()
               // Log.e("lat",lat.toString()+" "+log.toString())
            }
        }
    }

    override fun onStartTracker() {
        startTracker = true
    }

    override fun onItemClick(view: View, position: Int) {
        val intent = Intent(this@MainActivity,ProviderDetailsActivity::class.java)
        intent.putExtra("id",providerModels.get(position).id)
        intent.putExtra("phone",providerModels.get(position).phone)
        intent.putExtra("image",providerModels.get(position).avatar)
        startActivity(intent)

    }

    override val layoutResource: Int
        get() = R.layout.activity_main
    internal lateinit var geocoder: Geocoder
    internal lateinit var gps: GPSTracker
    internal var startTracker = false
    lateinit var about_app:LinearLayout
    lateinit var terms:LinearLayout
    lateinit var call_us:LinearLayout
    lateinit var settings:LinearLayout
    lateinit var chats:LinearLayout
    lateinit var orders:LinearLayout
    lateinit var profile:LinearLayout
    lateinit var notification:ImageView
    lateinit var image:CircleImageView
    lateinit var name:TextView
    lateinit var address:TextView
    lateinit var menu:ImageView
    lateinit var notify:ImageView
    lateinit var photo:CircleImageView
    lateinit var user_name:TextView
    lateinit var date:TextView
    lateinit var order_now:Button
    lateinit var search:ImageView
    lateinit var search_text:EditText
    lateinit var rv_recycle: RecyclerView
    internal var layNoInternet: RelativeLayout? = null
    internal var layNoItem: RelativeLayout? = null
    internal var tvNoContent: TextView? = null
    lateinit var whats:LinearLayout
    var refresh : SwipeRefreshLayout?=null
    internal lateinit var linearLayoutManager: LinearLayoutManager
    internal var providerModels = java.util.ArrayList<ProviderModel>()
    internal lateinit var providersAdapter: ProvidersAdapter
    lateinit var home:LinearLayout
    lateinit var logout:Button
    lateinit var load:ImageView
    lateinit var wallet:LinearLayout
    var mLang = ""
    var mLat = ""
    var result = ""
    var what = ""
    private var mAlertDialog: AlertDialog? = null
    var deviceID = ""

    private lateinit var timer: Timer
    private val noDelay = 0L
    private val everyFiveSeconds = 60000L

    override fun onResume() {
        super.onResume()

        val timerTask = object : TimerTask() {
            override fun run() {
                runOnUiThread {
                    Log.e("rrr","rrrrrr")
                    getLocationWithPermission() }
            }
        }

        timer = Timer()
        timer.schedule(timerTask, noDelay, everyFiveSeconds)
    }

    override fun onPause() {
        super.onPause()

//        timer.cancel()
//        timer.purge()
    }
    @RequiresApi(Build.VERSION_CODES.O)
    override fun initializeComponents() {
        deviceID = FirebaseInstanceId.getInstance().token.toString()
        getLocationWithPermission()
        menu = findViewById(R.id.menu)
        notify = findViewById(R.id.notify)
        photo = findViewById(R.id.photo)
        user_name = findViewById(R.id.user_name)
        date = findViewById(R.id.date)
        order_now = findViewById(R.id.order_now)
        search = findViewById(R.id.search)
        search_text = findViewById(R.id.search_text)
        load = findViewById(R.id.load)
        notify.setOnClickListener { startActivity(Intent(this,NotificationActivity::class.java)) }
        Profile()
       sideMenu()


        Glide.with(mContext).asBitmap().load(mSharedPrefManager.userData.avatar).into(photo)
        user_name.text = mSharedPrefManager.userData.name!!
        val calendar = Calendar.getInstance()
       Log.e("time",calendar.time.toString().format(Locale("ar"),"EEEE ,dd MMMM YYYY"))

        date.text = LocalDateTime.now().format(DateTimeFormatter.ofPattern("EEEE ,dd MMMM YYYY")).toString()
       // date.text = LocalDate.now().format(DateTimeFormatter.ofPattern("EEEE ,dd MMMM YYYY"))
        rv_recycle = findViewById(R.id.rv_recycle)
        layNoInternet = findViewById(R.id.lay_no_internet)
        layNoItem = findViewById(R.id.lay_no_item)
        tvNoContent = findViewById(R.id.tv_no_content)

        refresh = findViewById(R.id.refresh)
        linearLayoutManager = LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false)
        providersAdapter =  ProvidersAdapter(mContext,providerModels,R.layout.recycler_providers)
        providersAdapter.setOnItemClickListener(this)
        rv_recycle!!.layoutManager= linearLayoutManager
        rv_recycle!!.adapter = providersAdapter
//        swipeRefresh!!.setColorSchemeResources(
//            R.color.colorPrimary,
//            R.color.colorPrimaryDark,
//            R.color.colorAccent
//        )
//        swipeRefresh!!.setOnRefreshListener {
//            getHome(null)
//            search_text.setText("")
//        }
       // swipeRefresh!!.visibility = View.GONE
        refresh!!.setColorSchemeResources(
            R.color.colorPrimary,
            R.color.colorPrimaryDark,
            R.color.colorAccent
        )
        refresh!!.setOnRefreshListener {
            getHome(null)
            search_text.setText("")
        }
        getHome(null)
        order_now.setOnClickListener {
            getLocationWithPermission()
            Log.e("GPS",gps.getLatitude().toString()+" "+gps.getLongitude().toString())
            if (gps.getLatitude().toInt()!=0 ){
                if (gps.getLongitude().toInt()!=0){
                    val intent = Intent(this,LoadActivity::class.java)
                    intent.putExtra("lat",gps.getLatitude().toString())
                    intent.putExtra("lng",gps.getLongitude().toString())
                    intent.putExtra("address",result)
                    startActivity(intent)
                    finish()
                }

            }else{
                val intent = Intent(this,LoadActivity::class.java)
                intent.putExtra("lat",mSharedPrefManager.userData.lat)
                intent.putExtra("lng",mSharedPrefManager.userData.lng)
                intent.putExtra("address",mSharedPrefManager.userData.address)
                startActivity(intent)
                finish()
            }
           }
        search_text.setOnClickListener {
            search_text.requestFocus()
        }
        search.setOnClickListener {
            if (search_text.text.toString().equals("")){
                getHome(null)

            }else{

                getHome(search_text.text.toString())
            }
        }
        load.setOnClickListener {
            search_text.setText("")

            getHome(null)

        }


    }
    fun AppCompatActivity.hideKeyboard() {
        val view = this.currentFocus
        if (view != null) {
            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }
        // else {
        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
        // }
    }

    fun  Profile(){

        Client.getClient()?.create(Service::class.java)?.getProfile(mSharedPrefManager.userData.id!!,mLanguagePrefManager.appLanguage)?.enqueue(object :
            Callback<UserResponse>{
            override fun onFailure(call: Call<UserResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(call: Call<UserResponse>, response: Response<UserResponse>) {
                hideProgressDialog()
                if (response.body()?.value.equals("1")){
                    mSharedPrefManager.userData = response.body()?.data!!
                }else{
                    CommonUtil.makeToast(mContext,response.body()?.msg!!)
                }
            }

        })
        Client.getClient()?.create(Service::class.java)?.Whats()?.enqueue(object :
            Callback<AboutAppResponse>{
            override fun onFailure(call: Call<AboutAppResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(call: Call<AboutAppResponse>, response: Response<AboutAppResponse>) {
                hideProgressDialog()
                if (response.body()?.value.equals("1")){
                    what = response.body()?.data!!
                }else{
                    CommonUtil.makeToast(mContext,response.body()?.msg!!)
                }
            }

        })

    }

    fun openWhatsAppConversationUsingUri(
        context: Context,
        numberWithCountryCode: String,
        message: String
    ) {

        val uri =
            Uri.parse("https://api.whatsapp.com/send?phone=$numberWithCountryCode&text=$message")

        val sendIntent = Intent(Intent.ACTION_VIEW, uri)

        context.startActivity(sendIntent)
    }
    fun getHome(search:String?){
        getLocationWithPermission()
        showProgressDialog(getString(R.string.please_wait))
        layNoInternet!!.visibility = View.GONE
        layNoItem!!.visibility = View.GONE
        rv_recycle!!.visibility = View.GONE
        hideKeyboard()
        Client.getClient()?.create(Service::class.java)?.Providers(mSharedPrefManager.userData.id!!,gps.latitude.toString()!!,gps.longitude.toString()!!,search)?.enqueue(object :
            Callback<ProvidersResponse> {
            override fun onResponse(call: Call<ProvidersResponse>, response: Response<ProvidersResponse>) {

                refresh!!.isRefreshing = false
                hideProgressDialog()
                if (response.isSuccessful) {
                        if (response.body()?.value.equals("1")) {
                            Log.e("myJobs", Gson().toJson(response.body()!!.data))
                            if (response.body()!!.data?.isEmpty()!!) {
                                rv_recycle!!.visibility = View.GONE
                                layNoItem!!.visibility = View.VISIBLE
                                layNoInternet!!.visibility = View.GONE
                                tvNoContent!!.setText("لا يوجد مقدمى خدمة بالقرب منك")

                            } else {
                                rv_recycle!!.visibility = View.VISIBLE
                                providersAdapter.updateAll(response.body()!!.data!!)
                            }
//
                        }else {
                            CommonUtil.makeToast(mContext,response.body()?.msg!!)
                        }

                      }
            }
            override fun onFailure(call: Call<ProvidersResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                layNoInternet!!.visibility = View.VISIBLE
                layNoItem!!.visibility = View.GONE
                refresh!!.isRefreshing = false
                hideProgressDialog()
                Log.e("response", Gson().toJson(t))
            }
        })


    }
    fun sideMenu(){
        drawer_layout.useCustomBehavior(Gravity.START)
        drawer_layout.useCustomBehavior(Gravity.END)
        drawer_layout.setRadius(Gravity.START, 50f)
        drawer_layout.setRadius(Gravity.END, 50f)
        drawer_layout.setViewScale(Gravity.START, 0.95f) //set height scale for main view (0f to 1f)
        drawer_layout.setViewScale(Gravity.END, 0.95f) //set height scale for main view (0f to 1f)
        drawer_layout.setViewElevation(Gravity.START, 0f) //set main view elevation when drawer open (dimension)
        drawer_layout.setViewElevation(Gravity.END, 0f) //set main view elevation when drawer open (dimension)
//        drawer_layout.setViewScrimColor(Gravity.START, Color.) //set drawer overlay coloe (color)
//        drawer_layout.setViewScrimColor(Gravity.END, Color.TRANSPARENT) //set drawer overlay coloe (color)
        drawer_layout.setDrawerElevation(Gravity.START, 50f) //set drawer elevation (dimension)
        drawer_layout.setDrawerElevation(Gravity.END, 50f) //set drawer elevation (dimension)
        drawer_layout.setContrastThreshold(3f) //set maximum of contrast ratio between white text and background color.
        drawer_layout.setRadius(Gravity.START, 0f) //set end container's corner radius (dimension)

        drawer_layout.setRadius(Gravity.END, 0f)
        about_app = drawer_layout.findViewById(R.id.about_app)
        about_app.setOnClickListener { startActivity(Intent(this@MainActivity,AboutAppActivity::class.java))
        finish()}
        terms = drawer_layout.findViewById(R.id.terms)
        terms.setOnClickListener { val intent = Intent(this,TermsActivity::class.java)
            intent.putExtra("type","two")
            startActivity(intent)
            finish()}
        call_us = drawer_layout.findViewById(R.id.call_us)
        call_us.setOnClickListener { startActivity(Intent(this,CallUsActivity::class.java))
            finish()
        }
        whats = drawer_layout.findViewById(R.id.whats)
        whats.setOnClickListener { what.replaceFirst("0","+966",false)
            openWhatsAppConversationUsingUri(mContext!!,what,"") }
        wallet = drawer_layout.findViewById(R.id.wallet)
        wallet.setOnClickListener { startActivity(Intent(this,WalletActivity::class.java))
            finish()}
        settings = drawer_layout.findViewById(R.id.settings)
        settings.setOnClickListener { startActivity(Intent(this,SettingsActivity::class.java))
            finish()}
        chats = drawer_layout.findViewById(R.id.chats)
        chats.setOnClickListener { startActivity(Intent(this,ChatsActivity::class.java))
            finish()}
        profile = drawer_layout.findViewById(R.id.profile)
        profile.setOnClickListener { startActivity(Intent(this,ProfileActivity::class.java))
            finish()}
//        notification = drawer_layout.findViewById(R.id.notification)
//        notification.setOnClickListener { startActivity(Intent(this,NotificationActivity::class.java)) }
        image = drawer_layout.findViewById(R.id.image)
        name = drawer_layout.findViewById(R.id.name)
        address = drawer_layout.findViewById(R.id.address)
        Glide.with(mContext).asBitmap().load(mSharedPrefManager.userData.avatar).into(image)
        name.text = mSharedPrefManager.userData.name!!
        address.text = mSharedPrefManager.userData.email!!
        menu.setOnClickListener { drawer_layout.openDrawer(GravityCompat.START) }
        orders = drawer_layout.findViewById(R.id.orders)
        orders.setOnClickListener { startActivity(Intent(this,OrdersActivity::class.java))
            finish()}
        home = drawer_layout.findViewById(R.id.home)
        home.setOnClickListener { drawer_layout.closeDrawer(GravityCompat.START) }
        logout = drawer_layout.findViewById(R.id.logout)
       logout.setOnClickListener { logout() }
    }

    fun logout(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.logOut(mSharedPrefManager.userData.id!!,deviceID,"android",mLanguagePrefManager.appLanguage)?.enqueue(object :
            Callback<AboutAppResponse> {
            override fun onFailure(call: Call<AboutAppResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(
                call: Call<AboutAppResponse>,
                response: Response<AboutAppResponse>
            ) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        mSharedPrefManager.loginStatus=false
                        mSharedPrefManager.Logout()
                        CommonUtil.makeToast(applicationContext,response.body()?.data!!)
                        startActivity(Intent(this@MainActivity, SplashActivity::class.java))
                        finish()
                    }else{
                        CommonUtil.makeToast(applicationContext,response.body()?.msg!!)

                    }
                }
            }
        })
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finishAffinity()
    }
    fun getLocationWithPermission() {
        gps = GPSTracker(mContext, this)
        if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
            if (!(PermissionUtils.hasPermissions(mContext, Manifest.permission.ACCESS_FINE_LOCATION)&&
                        (PermissionUtils.hasPermissions(mContext,
                            Manifest.permission.ACCESS_COARSE_LOCATION)))) {
                CommonUtil.PrintLogE("Permission not granted")
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(
                        PermissionUtils.GPS_PERMISSION,
                        800
                    )
                    Log.e("GPS", "1")
                }
            } else {
                getCurrentLocation()
                Log.e("GPS", "2")
            }
        } else {
            Log.e("GPS", "3")
            getCurrentLocation()
        }

    }

    internal fun getCurrentLocation() {
        gps.getLocation()
        if (!gps.canGetLocation()) {
            mAlertDialog = DialogUtil.showAlertDialog(mContext,
                getString(R.string.gps_detecting),
                DialogInterface.OnClickListener { dialogInterface, i ->
                    mAlertDialog?.dismiss()
                    val intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                    startActivityForResult(intent, 300)
                })
        } else {
            if (gps.getLatitude() !== 0.0 && gps.getLongitude() !== 0.0) {
                mLat = gps.getLatitude().toString()
                mLang = gps.getLongitude().toString()
                val addresses: List<Address>
                geocoder = Geocoder(this, Locale.getDefault())
                try {
                    addresses = geocoder.getFromLocation(
                        java.lang.Double.parseDouble(mLat),
                        java.lang.Double.parseDouble(mLang),
                        1
                    )
                    if (addresses.isEmpty()) {
                        Toast.makeText(
                            mContext,
                            resources.getString(R.string.detect_location),
                            Toast.LENGTH_SHORT
                        ).show()
                    } else {
                        result = addresses[0].getAddressLine(0)
                        Log.e("address",result)

                        Client.getClient()?.create(Service::class.java)?.updateLat(mSharedPrefManager.userData.id!!,mLanguagePrefManager.appLanguage
                            ,result,mLat,mLang)?.enqueue(object :Callback<UserResponse>{
                            override fun onFailure(call: Call<UserResponse>, t: Throwable) {
                                CommonUtil.handleException(mContext,t)
                                t.printStackTrace()

                            }

                            override fun onResponse(call: Call<UserResponse>, response: Response<UserResponse>) {

                                if (response.isSuccessful){
                                    if (response.body()?.value.equals("1")){

                                        mSharedPrefManager.userData = response.body()?.data!!

                                    }else{

                                    }
                                }
                            }

                        })



                    }


                } catch (e: IOException) {
                }



            }
        }
    }


}