package com.aait.mughasilclient.UI.Activities

import android.content.Intent
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import com.aait.mughasilclient.Base.Parent_Activity
import com.aait.mughasilclient.R

class WalletActivity:Parent_Activity() {
    override val layoutResource: Int
        get() = R.layout.activity_wallet
    lateinit var back: ImageView
    lateinit var menu: ImageView
    lateinit var title: TextView
    lateinit var button: Button
    override fun initializeComponents() {
        button = findViewById(R.id.button)
        title = findViewById(R.id.title)
        title.text = "الدفع"
        menu = findViewById(R.id.menu)
        back = findViewById(R.id.back)
        back.visibility = View.GONE
        menu.setOnClickListener { startActivity(Intent(this,MainActivity::class.java))
            finish()}
        back.setOnClickListener { startActivity(Intent(this,NotificationActivity::class.java))
            finish()}
        button.setOnClickListener { startActivity(Intent(this,MainActivity::class.java))
            finish() }
    }
}