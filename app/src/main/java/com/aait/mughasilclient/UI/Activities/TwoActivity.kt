package com.aait.mughasilclient.UI.Activities

import android.content.Intent
import android.widget.ImageView
import android.widget.TextView
import com.aait.mughasilclient.Base.Parent_Activity
import com.aait.mughasilclient.R

class TwoActivity:Parent_Activity() {
    override val layoutResource: Int
        get() = R.layout.activity_two

    lateinit var skip: TextView
    lateinit var next: ImageView
    override fun initializeComponents() {

        skip = findViewById(R.id.skip)
        next = findViewById(R.id.next)

        skip.setOnClickListener {
            startActivity(Intent(this@TwoActivity,LoginActivity::class.java))
            finish()
             }
        next.setOnClickListener { startActivity(Intent(this@TwoActivity,ThreeActivity::class.java))
        finish()}
    }
}