package com.aait.mughasilclient.UI.Activities

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.LinearLayout
import com.aait.mughasilclient.Base.Parent_Activity
import com.aait.mughasilclient.R
import com.facebook.*
import com.facebook.login.LoginResult
import com.facebook.login.widget.LoginButton
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.tasks.Task
import com.google.gson.Gson
import org.json.JSONObject
import java.util.*

class GoogleActivity:Parent_Activity() {
    override val layoutResource: Int
        get() = R.layout.activity_google

    lateinit var phone:Button
    lateinit var google:LinearLayout
    lateinit var face:LinearLayout
    lateinit var loginbutton:LoginButton
    lateinit var mGoogleSignInClient: GoogleSignInClient
    lateinit var callbackManager: CallbackManager
    override fun initializeComponents() {
        phone = findViewById(R.id.phone)
        google = findViewById(R.id.google)
        face = findViewById(R.id.face)
        loginbutton = findViewById(R.id.login_button)
        phone.setOnClickListener { startActivity(Intent(this,LoginActivity::class.java)) }
        callbackManager = CallbackManager.Factory.create()
        loginbutton.setReadPermissions(
            Arrays.asList(
                "public_profile", "email", "user_birthday", "user_friends"))
        callbackManager = CallbackManager.Factory.create()
        loginbutton.registerCallback(callbackManager,object : FacebookCallback<LoginResult> {
            override fun onSuccess(result: LoginResult?) {
                Log.e("result",result.toString())
                Log.e("face",result?.accessToken?.userId!!)
                val request = GraphRequest.newMeRequest(
                    result?.getAccessToken()
                    ,object : GraphRequest.GraphJSONObjectCallback{
                        override fun onCompleted(`object`: JSONObject?, response: GraphResponse?) {
                            Log.v("LoginActivity", response.toString())
                            var email =`object`?.getString("email")
                            var name = `object`?.getString("name")
                            Log.e("user",email+"  "+name)
                            val intent  = Intent(this@GoogleActivity,RegisterActivity::class.java)
                            intent.putExtra("mail",email)
                            intent.putExtra("name",name)
                            startActivity(intent)
                        }


                    }
                )
                val parameters = Bundle()
                parameters.putString("fields", "id,name,email,gender,birthday")
                request?.setParameters(parameters)
                request?.executeAsync()
            }

            override fun onCancel() {

            }

            override fun onError(error: FacebookException?) {
                Log.e("error",error.toString())

            }

        })

        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestEmail()
            .build()
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso)
        Log.e("google", Gson().toJson(gso.account))
        val account = GoogleSignIn.getLastSignedInAccount(this@GoogleActivity)
        Log.e("account", Gson().toJson(account))
        if ( account!=null){
            updateUI(account)
        }else {

        }
        google.setOnClickListener {
            val signInIntent = mGoogleSignInClient.signInIntent
            startActivityForResult(signInIntent, 1)
        }

    }

    fun updateUI( account: GoogleSignInAccount) {
        try {

            val  Name = account?.displayName
            val Email = account?.email
            Log.e("data",Name+"  " +Email)
            val intent  = Intent(this@GoogleActivity,RegisterActivity::class.java)
            intent.putExtra("mail",account?.email)
            intent.putExtra("name",account?.displayName)
            startActivity(intent)



        } catch ( ex:Exception) {
            Log.e("ex",ex.toString())
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 1) {
            // The Task returned from this call is always completed, no need to attach
            // a listener.
            // Log.e("vvv",Gson().toJson(data))
            val task = GoogleSignIn.getSignedInAccountFromIntent(data)
            handleSignInResult(task)



        }else {
            callbackManager.onActivityResult(requestCode, resultCode, data)
            // Log.e("face", Gson().toJson(data))
        }

    }
    private fun handleSignInResult(completedTask: Task<GoogleSignInAccount>) {
        try {
            val account = completedTask.getResult(ApiException::class.java)

            // Signed in successfully, show authenticated UI.


            updateUI(account!!)

        } catch (e: ApiException) {
            // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.
            Log.w("tag", "signInResult:failed code=" + e.statusCode)

        }

    }
}