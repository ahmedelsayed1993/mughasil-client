package com.aait.mughasilclient.UI.Activities

import android.content.Intent
import android.widget.ImageView
import com.aait.mughasilclient.Base.Parent_Activity
import com.aait.mughasilclient.R

class ThreeActivity:Parent_Activity() {
    override val layoutResource: Int
        get() = R.layout.activity_three
    lateinit var next: ImageView
    override fun initializeComponents() {
        next = findViewById(R.id.next)
        next.setOnClickListener { startActivity(Intent(this@ThreeActivity,LoginActivity::class.java))
        finish()}
    }
}