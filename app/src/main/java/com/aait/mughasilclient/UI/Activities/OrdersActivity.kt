package com.aait.mughasilclient.UI.Activities

import android.content.Intent
import android.widget.ImageView
import android.widget.TextView
import androidx.viewpager.widget.ViewPager
import com.aait.mughasilclient.Base.Parent_Activity
import com.aait.mughasilclient.R
import com.aait.mughasilclient.UI.Adapters.SubscribeTapAdapter
import com.google.android.material.tabs.TabLayout

class OrdersActivity:Parent_Activity() {
    override val layoutResource: Int
        get() = R.layout.activity_orders
    lateinit var title: TextView
    lateinit var back: ImageView
    lateinit var orders: TabLayout
    lateinit var ordersViewPager: ViewPager
    private var mAdapter: SubscribeTapAdapter? = null
    lateinit var menu:ImageView

    override fun initializeComponents() {
        title = findViewById(R.id.title)
        back = findViewById(R.id.back)
        menu = findViewById(R.id.menu)
        menu.setOnClickListener { startActivity(Intent(this@OrdersActivity,MainActivity::class.java))
        finish()}
        back.setOnClickListener { startActivity(Intent(this@OrdersActivity,NotificationActivity::class.java))
            finish()}
        title.text = getString(R.string.orders)
        orders = findViewById(R.id.orders)
        ordersViewPager = findViewById(R.id.ordersViewPager)
        mAdapter = SubscribeTapAdapter(mContext,supportFragmentManager)
        ordersViewPager.setAdapter(mAdapter)
        orders!!.setupWithViewPager(ordersViewPager)

    }
}