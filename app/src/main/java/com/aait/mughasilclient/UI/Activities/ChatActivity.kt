package com.aait.mughasilclient.UI.Activities

import android.content.*
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.widget.EditText
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

import com.aait.mughasilclient.Base.Parent_Activity
import com.aait.mughasilclient.Client
import com.aait.mughasilclient.Models.ChatModel
import com.aait.mughasilclient.Models.ChatResponse
import com.aait.mughasilclient.Models.SendChatResponse

import com.aait.mughasilclient.Network.Service
import com.aait.mughasilclient.R
import com.aait.mughasilclient.UI.Adapters.ChatAdapter

import com.aait.mughasilclient.Uitls.CommonUtil
import com.bumptech.glide.Glide
import de.hdodenhof.circleimageview.CircleImageView
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.ArrayList

class ChatActivity :Parent_Activity(){
    override val layoutResource: Int
        get() = R.layout.activity_chat
    var id = 0
    var receiver = 0
    lateinit var message:EditText
    lateinit var send:ImageView
    lateinit var rv_recycle: RecyclerView
    internal var layNoInternet: RelativeLayout? = null
    var mActive = false
    internal var sharedPreferences: SharedPreferences? = null
    internal lateinit var MsgReciever: BroadcastReceiver
    internal var layNoItem: RelativeLayout? = null
    internal var layProgress: RelativeLayout? = null
    internal var tvNoContent: TextView? = null

    internal lateinit var adapter: RecyclerView.Adapter<*>
    internal var messageModels: MutableList<ChatModel> = ArrayList<ChatModel>()


    private var isLoading = false

    // If current page is the last page (Pagination will stop after this page load)
    private var isLastPage = false

    // total no. of pages to load. Initial load is page 0, after which 2 more pages will load.
    private val TOTAL_PAGES: Int = 0

    // indicates the current page which Pagination is fetching.
    private var currentPage: Int = 0

    var lastpage: Int = 0

    internal lateinit var linearLayout: LinearLayoutManager


    //boolean isLoading, isLastPage;
    internal var PAGE = 0
    internal var PAGE_NUM = 0
    lateinit var image:CircleImageView
    lateinit var title:TextView
    lateinit var back:ImageView
    var m = ""

    override fun initializeComponents() {
        id = intent.getIntExtra("id",0)
        receiver = intent.getIntExtra("receiver",0)
        lastpage = intent.getIntExtra("lastpage",0)
        message = findViewById(R.id.message)
        send = findViewById(R.id.send)
        rv_recycle = findViewById(R.id.rv_recycle)
        layNoInternet = findViewById(R.id.lay_no_internet)
        layNoItem = findViewById(R.id.lay_no_item)
        tvNoContent = findViewById(R.id.tv_no_content)
        layProgress = findViewById(R.id.lay_progress)
        image = findViewById(R.id.image)
        title = findViewById(R.id.title)
        back = findViewById(R.id.back)
        back.setOnClickListener { onBackPressed()
        finish()}
        PAGE = lastpage
        PAGE_NUM = lastpage
        linearLayout = LinearLayoutManager(this)
        linearLayout.stackFromEnd = true
     // linearLayout.setReverseLayout(true)
        rv_recycle.setLayoutManager(linearLayout)
        messageModels = ArrayList()
        adapter = ChatAdapter(mContext, messageModels, mSharedPrefManager.userData.id!!)
        rv_recycle.setAdapter(adapter)
        loadMoreItems()
        currentPage = lastpage
        rv_recycle.addOnScrollListener(recyclerViewOnScrollListener)
        MsgReciever = object : BroadcastReceiver() {
            override fun onReceive(context: Context, intent: Intent) {
                //  Toast.makeText(ChatActivity.this, "broad", Toast.LENGTH_SHORT).show();
                val chatModel1 = ChatModel()
                chatModel1.avatar = ""
                chatModel1.conversation_id = 0
                chatModel1.created = ""
                chatModel1.message = ""
                chatModel1.receiver_id = 0
                chatModel1.user_id = 0
                chatModel1.id = 0

                if (intent.action!!.equals("new_message", ignoreCase = true)) {
                    //  Toast.makeText(context, "broad2", Toast.LENGTH_SHORT).show();


                    chatModel1.message=(intent.getStringExtra("msg"))
                    chatModel1.id=(Integer.parseInt(intent.getStringExtra("id")!!))
                    chatModel1.receiver_id = (Integer.parseInt(intent.getStringExtra("receiver_id")!!))
                    chatModel1.conversation_id = (Integer.parseInt(intent.getStringExtra("conversation_id")!!))
                    chatModel1.avatar = (intent.getStringExtra("avatar"))
                    chatModel1.created = (intent.getStringExtra("created"))
                    chatModel1.user_id = (Integer.parseInt(intent.getStringExtra("user_id")!!))
                    messageModels.add(chatModel1)
                    adapter = ChatAdapter(
                        mContext,
                        messageModels,
                        mSharedPrefManager.userData.id!!
                    )
                    rv_recycle.setAdapter(adapter)
                    // friend_id = intent.getIntExtra("user",0);
                }
            }
        }

        send.setOnClickListener {


            SendMessage()
        }



    }
     override fun onResume() {
        super.onResume()
        val editor = getSharedPreferences("home", MODE_PRIVATE).edit()

        editor.putString("sender_id", id.toString())

        editor.apply()
        LocalBroadcastManager.getInstance(this)
            .registerReceiver(MsgReciever, IntentFilter("new_message"))
    }


     override fun onStop() {
        super.onStop()
        val editor = getSharedPreferences("home", MODE_PRIVATE).edit()
        editor.putString("sender_id", "0")
        editor.apply()
    }


    private fun loadMoreItems() {
        isLoading = true
        Client.getClient()?.create(Service::class.java)?.Conversation(
            mLanguagePrefManager.appLanguage,id,
            mSharedPrefManager.userData.id!!,
            PAGE_NUM,
            "android"
        )?.enqueue(object :
            Callback<ChatResponse> {
            override fun onResponse(call: Call<ChatResponse>, response: Response<ChatResponse>) {
                isLoading = false
                if (response.isSuccessful()) {
                    layProgress!!.setVisibility(View.GONE)
                    try {
                        if (response.body()!!.key.equals("1")) {
                            Glide.with(mContext).asBitmap().load(response.body()?.user_data?.avatar).into(image)
                            title.text = response.body()?.user_data?.username
                            messageModels.addAll(0, response.body()?.data!!)
                            adapter.notifyDataSetChanged()
                            if (PAGE_NUM == PAGE) {
                                rv_recycle.scrollToPosition(linearLayout.itemCount - 1)
                            } else {
                                rv_recycle.scrollToPosition(response.body()?.data?.size!! - 1 + linearLayout.childCount)
                            }
                            PAGE_NUM--
                            if (PAGE_NUM == 0) {
                                isLastPage = true
                            }

                        } else {
                            CommonUtil.makeToast(mContext, response.body()?.msg!!)
                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }

                } else {
                    CommonUtil.makeToast(mContext, getString(R.string.connection_error))
                }
            }

            override fun onFailure(call: Call<ChatResponse>, t: Throwable) {
                isLoading = false
                layProgress!!.setVisibility(View.GONE)
                if (PAGE_NUM == PAGE) {
                    layNoInternet!!.setVisibility(View.VISIBLE)
                } else {
                    CommonUtil.makeToast(mContext, getString(R.string.connection_error))
                }
            }
        })


    }

    private val recyclerViewOnScrollListener = object : RecyclerView.OnScrollListener() {
        override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
            super.onScrollStateChanged(recyclerView, newState)
        }

        override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
            super.onScrolled(recyclerView, dx, dy)
            val firstVisibleItemPosition = linearLayout.findFirstVisibleItemPosition()

            if (!isLoading && !isLastPage) {
                if (firstVisibleItemPosition < 2) {//&& totalItemCount >= 20
                    layProgress!!.setVisibility(View.VISIBLE)
                    loadMoreItems()
                }
            }
        }
    }

    fun SendMessage(){
        if (!message.text.toString().equals("")){
            send.isEnabled = false
            Client.getClient()?.create(Service::class.java)?.Send("ar",mSharedPrefManager.userData.id!!,receiver,id,message.text.toString())?.enqueue(
                object :Callback<SendChatResponse>{
                    override fun onFailure(call: Call<SendChatResponse>, t: Throwable) {
                        Log.i("exception", "exception")
                        send.isEnabled = true
                        Log.e("errpr", t.toString())
                        CommonUtil.handleException(this@ChatActivity, t)
                        t.printStackTrace()
                    }

                    override fun onResponse(
                        call: Call<SendChatResponse>,
                        response: Response<SendChatResponse>
                    ) {

                        send.isEnabled = true
                        if (response.isSuccessful){
                            if (response.body()?.value.equals("1")){

                                val model = ChatModel()
                                model.message = response.body()?.data?.message
                                model.created =(response.body()?.data?.created)
                                model.conversation_id = response.body()?.data?.conversation_id
                                model.avatar = response.body()?.data?.avatar
                                model.id = response.body()?.data?.id
                                model.receiver_id = response.body()?.data?.receiver_id
                                model.user_id = response.body()?.data?.user_id


                                messageModels.add(response.body()?.data!!)
                                adapter = ChatAdapter(
                                    this@ChatActivity,
                                    messageModels,
                                    mSharedPrefManager.userData.id!!
                                )
                                rv_recycle.setAdapter(adapter)
                                adapter.notifyDataSetChanged()

                                message.setText("")
                            }
                        }
                    }
                }
            )
        }
    }
}