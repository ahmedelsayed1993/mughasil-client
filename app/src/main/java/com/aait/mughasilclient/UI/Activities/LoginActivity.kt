package com.aait.mughasilclient.UI.Activities

import android.content.Intent
import android.util.Log
import android.widget.*
import com.aait.mughasilclient.Base.Parent_Activity
import com.aait.mughasilclient.Client
import com.aait.mughasilclient.Models.UserResponse
import com.aait.mughasilclient.Network.Service
import com.aait.mughasilclient.R
import com.aait.mughasilclient.Uitls.CommonUtil
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.firebase.iid.FirebaseInstanceId
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.gson.Gson
import android.R.attr.data
import android.os.Bundle
import android.util.Log.w
import com.google.android.gms.common.api.ApiException

import androidx.fragment.app.FragmentActivity
import com.facebook.*
import com.facebook.login.widget.LoginButton
import com.google.android.gms.tasks.Task
import com.facebook.login.LoginResult
import com.facebook.login.LoginManager
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.Api
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.common.internal.GoogleApiAvailabilityCache
import io.fotoapparat.parameter.AntiBandingMode
import org.json.JSONObject
import java.util.*


class LoginActivity:Parent_Activity(),GoogleApiClient.OnConnectionFailedListener {
    override fun onConnectionFailed(p0: ConnectionResult) {

    }
    override val layoutResource: Int
        get() = R.layout.activity_login
    lateinit var register:LinearLayout
    lateinit var forgot_pass:TextView
    lateinit var phone:EditText
    lateinit var password:EditText
    lateinit var login:Button
    var deviceID = ""
//    lateinit var mGoogleSignInClient:GoogleSignInClient
//    lateinit var google:ImageView
//    lateinit var loginButton: LoginButton
//    lateinit var callbackManager:CallbackManager
    override fun initializeComponents() {
        deviceID = FirebaseInstanceId.getInstance().token.toString()
        phone = findViewById(R.id.phone)
        password = findViewById(R.id.password)
        login = findViewById(R.id.login)
        register = findViewById(R.id.register)
        forgot_pass = findViewById(R.id.forgot_pass)
       // google = findViewById(R.id.google)
        //val gio = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).requestEmail().build()
        //googleApiClient = GoogleApiClient.Builder(this).enableAutoManage(this,this).addApi(Api.AbstractClientBuilder).build()
        register.setOnClickListener {  val intent  = Intent(this@LoginActivity,RegisterActivity::class.java)
            intent.putExtra("mail","")
            intent.putExtra("name","")
            startActivity(intent)}
        forgot_pass.setOnClickListener { startActivity(Intent(this,ForgotPasswordActivity::class.java)) }
       // callbackManager = CallbackManager.Factory.create()
//         loginButton = findViewById(R.id.login_button)
//         loginButton.setReadPermissions(Arrays.asList(
//            "public_profile", "email", "user_birthday", "user_friends"))
//        callbackManager = CallbackManager.Factory.create()
//        loginButton.registerCallback(callbackManager,object : FacebookCallback<LoginResult> {
//            override fun onSuccess(result: LoginResult?) {
//                Log.e("result",result.toString())
//              Log.e("face",result?.accessToken?.userId!!)
//                 val request = GraphRequest.newMeRequest(
//                    result?.getAccessToken()
//                     ,object : GraphRequest.GraphJSONObjectCallback{
//                         override fun onCompleted(`object`: JSONObject?, response: GraphResponse?) {
//                             Log.v("LoginActivity", response.toString())
//                            var email =`object`?.getString("email")
//                             var name = `object`?.getString("name")
//                             Log.e("user",email+"  "+name)
//                             val intent  = Intent(this@LoginActivity,RegisterActivity::class.java)
//                             intent.putExtra("mail",email)
//                             intent.putExtra("name",name)
//                             startActivity(intent)
//                         }
//
//
//                     }
//                 )
//                val parameters = Bundle()
//                parameters.putString("fields", "id,name,email,gender,birthday")
//                request?.setParameters(parameters)
//                request?.executeAsync()
//            }
//
//            override fun onCancel() {
//
//            }
//
//            override fun onError(error: FacebookException?) {
//                Log.e("error",error.toString())
//
//            }
//
//        })
        login.setOnClickListener {
            if (CommonUtil.checkEditError(phone,getString(R.string.phone_number))||
                    CommonUtil.checkEditError(password,getString(R.string.password))){
                return@setOnClickListener
            }else{
                Sign_in()
            }
        }

//        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
//            .requestEmail()
//            .build()
//        mGoogleSignInClient = GoogleSignIn.getClient(this, gso)
//        Log.e("google",Gson().toJson(gso.account))
//        val account = GoogleSignIn.getLastSignedInAccount(this@LoginActivity)
//        Log.e("account",Gson().toJson(account))
//        if ( account!=null){
//            updateUI(account)
//        }else {
//
//        }
//            google.setOnClickListener {
//                val signInIntent = mGoogleSignInClient.signInIntent
//                startActivityForResult(signInIntent, 1)
//            }





    }

//    fun updateUI( account:GoogleSignInAccount) {
//       try {
//
//           val  Name = account?.displayName
//           val Email = account?.email
//           Log.e("data",Name+"  " +Email)
//           val intent  = Intent(this@LoginActivity,RegisterActivity::class.java)
//           intent.putExtra("mail",account?.email)
//           intent.putExtra("name",account?.displayName)
//           startActivity(intent)
//
//
//
//       } catch ( ex:Exception) {
//       Log.e("ex",ex.toString())
//        }
//    }

    fun Sign_in(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.SignIn(phone.text.toString(),password.text.toString(),deviceID,"android","ar")?.enqueue(object :
            Callback<UserResponse>{
            override fun onFailure(call: Call<UserResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }
            override fun onResponse(call: Call<UserResponse>, response: Response<UserResponse>) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        if (response.body()?.data?.user_type.equals("user")) {
                            val intent = Intent(this@LoginActivity, MainActivity::class.java)
                            startActivity(intent)
                            mSharedPrefManager.userData = response.body()?.data!!
                            mSharedPrefManager.loginStatus = true
                            // CommonUtil.makeToast(mContext,response.body()?.msg!!)
                            finish()
                        }
                        else{
                            CommonUtil.makeToast(mContext,"هذا ليس حساب مستخدم")
                        }
                    }else if (response.body()?.value.equals("3")){
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                        val intent  = Intent(this@LoginActivity,ActiviationActivity::class.java)
                        intent.putExtra("data",response.body()?.data)
                        startActivity(intent)
                        finish()
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }

//    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
//        super.onActivityResult(requestCode, resultCode, data)
//        if (requestCode == 1) {
//            // The Task returned from this call is always completed, no need to attach
//            // a listener.
//           // Log.e("vvv",Gson().toJson(data))
//            val task = GoogleSignIn.getSignedInAccountFromIntent(data)
//            handleSignInResult(task)
//
//
//
//        }else {
//            callbackManager.onActivityResult(requestCode, resultCode, data)
//           // Log.e("face", Gson().toJson(data))
//        }
//
//    }
//    private fun handleSignInResult(completedTask: Task<GoogleSignInAccount>) {
//        try {
//            val account = completedTask.getResult(ApiException::class.java)
//
//            // Signed in successfully, show authenticated UI.
//
//
//            updateUI(account!!)
//
//        } catch (e: ApiException) {
//            // The ApiException status code indicates the detailed failure reason.
//            // Please refer to the GoogleSignInStatusCodes class reference for more information.
//            w("tag", "signInResult:failed code=" + e.statusCode)
//
//        }
//
//    }
}