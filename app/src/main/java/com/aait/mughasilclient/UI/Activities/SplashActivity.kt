package com.aait.mughasilclient.UI.Activities

import android.content.Intent
import android.os.Handler
import android.view.animation.AnimationUtils
import android.widget.ImageView
import com.aait.mughasilclient.Base.Parent_Activity
import com.aait.mughasilclient.R

class SplashActivity : Parent_Activity() {
    override val layoutResource: Int
        get() = R.layout.activity_splash
    var isSplashFinishid = false

    lateinit var logo : ImageView
    override fun initializeComponents() {
        logo = findViewById(R.id.logo)


        val logoAnimation2 = AnimationUtils.loadAnimation(this, R.anim.anim_shine)

        Handler().postDelayed({
            logo.startAnimation(logoAnimation2)
            Handler().postDelayed({
                isSplashFinishid=true
                if (mSharedPrefManager.loginStatus==true){
                    startActivity(Intent(this,MainActivity::class.java))
                    finish()

                }else {

                    var intent = Intent(this@SplashActivity, OneActivity::class.java)
                    startActivity(intent)
                    finish()
                }

            }, 2100)
        }, 1800)
    }



}
