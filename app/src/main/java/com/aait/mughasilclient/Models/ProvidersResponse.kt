package com.aait.mughasilclient.Models

import java.io.Serializable

class ProvidersResponse:BaseResponse(),Serializable {
    var data:ArrayList<ProviderModel>?=null
}