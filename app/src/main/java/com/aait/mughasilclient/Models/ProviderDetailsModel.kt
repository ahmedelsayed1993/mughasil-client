package com.aait.mughasilclient.Models

import java.io.Serializable

class ProviderDetailsModel:Serializable {
    var name:String?= null
    var address:String?=null
}