package com.aait.mughasilclient.Models

import java.io.Serializable

class PaginationModel:Serializable {
    var currentPage:Int?=null
    var lastPage:Int?=null
    var perPage:Int?=null
    var total:Int?=null
}