package com.aait.mughasilclient.Models

import java.io.Serializable

class EditOrderModel:Serializable {
    var id:Int?=null
    var provider_id:Int?=null
    var user_id:Int?=null
    var address:String?=null
    var lat:String?=null
    var lng:String?=null
    var phone:String?=null
    var time_from:String?=null
    var time_to:String?=null
    var date:String?=null
}