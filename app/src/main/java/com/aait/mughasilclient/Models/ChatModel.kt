package com.aait.mughasilclient.Models

import java.io.Serializable

class ChatModel:Serializable {
    var id:Int?=null
    var avatar:String?=null
    var conversation_id:Int?=null
    var user_id:Int?=null
    var receiver_id:Int?=null
    var message:String?=null
    var created:String?=null
}