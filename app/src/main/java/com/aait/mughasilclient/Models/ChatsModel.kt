package com.aait.mughasilclient.Models

import java.io.Serializable

class ChatsModel:Serializable {
    var id:Int?=null
    var conversation_id:Int?=null
    var username:String?=null
    var avatar:String?=null
    var user_id:Int?=null
    var message:String?=null
    var seen:Int?=null
    var lastPage:Int?=null
    var date:String?=null
}