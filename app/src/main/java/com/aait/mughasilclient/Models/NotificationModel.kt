package com.aait.mughasilclient.Models

import java.io.Serializable

class NotificationModel:Serializable {
    var id:Int?= null
    var content:String?=null
    var order_id:Int?=null
    var created:String?=null
}