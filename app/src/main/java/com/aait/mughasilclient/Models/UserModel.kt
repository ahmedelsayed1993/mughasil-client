package com.aait.mughasilclient.Models

import java.io.Serializable

class UserModel:Serializable {
    var id:Int?=null
    var name:String?=null
    var phone:String?=null
    var code:String?=null
    var user_type:String?=null
    var email:String?=null
    var status:String?=null
    var device_id:String?=null
    var device_type:String?=null
    var avatar:String?=null
    var date:String?=null
    var another_number:String?=null
    var commercial:String?=null
    var address:String?=null
    var lat:String?=null
    var lng:String?=null
}