package com.aait.mughasilclient.Models

import java.io.Serializable

class ProviderModel:Serializable {
    var id:Int?=null
    var name:String?=null
    var address:String?=null
    var phone:String?=null
    var distance:String?=null
    var avatar:String?=null
}