package com.aait.mughasilclient.Models

import java.io.Serializable

class OrderDetailsModel:Serializable {
    var username:String?=null
    var address:String?=null
    var lat:String?=null
    var lng:String?=null
    var user_id:Int?=null
    var status:String?=null
    var time_from:String?=null
    var time_to:String?=null
    var date:String?=null
    var phone:String?=null
    var show_phone:Int?=null
}